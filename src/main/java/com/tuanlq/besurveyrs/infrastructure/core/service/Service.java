package com.tuanlq.besurveyrs.infrastructure.core.service;

import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

public interface Service<T extends Bo<K>, K extends Serializable> {
    List<T> find(T bo);

    Page<T> find(T bo, Pageable pageable);

    T get(T bo);

    T get(K key);

    T doInsert(T bo);

    T doUpdate(T bo);

    T doDelete(T bo);

    List<T> doPriority(List<T> objectList);
}

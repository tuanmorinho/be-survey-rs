package com.tuanlq.besurveyrs.infrastructure.core.service;

import com.tuanlq.besurveyrs.infrastructure.core.obj.PriorityObject;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractStatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractService<T extends Bo<K>, K extends Serializable> implements Service<T, K> {

    @Autowired
    protected Dao<T, K> dao;

    @Override
    public List<T> find(T bo) {
        return dao.findAll(Example.of(bo));
    }

    @Override
    public Page<T> find(T bo, Pageable pageable) {
        return dao.findAll(Example.of(bo), pageable);
    }

    @Override
    public T get(T bo) {
        if (bo == null) {
            throw new MyException("error.bo.empty");
        }
        return (bo.getId() == null ? dao.findOne(Example.of(bo)) : dao.findById(bo.getId())).orElseThrow(() -> new MyException("error.get"));
    }

    @Override
    public T get(K key) {
        if (key == null) {
            throw new MyException("error.bo.empty");
        }
        return dao.findById(key).orElseThrow(() -> new MyException("error.get"));
    }

    @Override
    public T doInsert(T bo) {
        if (bo instanceof StatusBo) {
            AbstractStatusBo statusBo = (AbstractStatusBo) bo;
            if (statusBo.getIsTrash() == null) {
                statusBo.setIsTrash(false);
            }
            if (statusBo.getStatus() == null) {
                statusBo.setStatus(1);
            }
        }
        dao.save(bo);
        return bo;
    }

    @Override
    public T doUpdate(T bo) {
        T dbBo = get(bo.getId());
        if (dbBo instanceof StatusBo) {
            AbstractStatusBo statusBo = (AbstractStatusBo) bo;
            statusBo.setStatus(((AbstractStatusBo) dbBo).getStatus());
            statusBo.setIsTrash(((AbstractStatusBo) dbBo).getIsTrash());
        }
        dbBo.copyData(bo);
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public T doDelete(T bo) {
        T dbBo = get(bo.getId());
        if (dbBo instanceof StatusBo && !((AbstractStatusBo) dbBo).getIsTrash()) {
            throw new MyException("error.delete.isTrash.false");
        }
        dao.delete(dbBo);
        return dbBo;
    }

    @Override
    public List<T> doPriority(List<T> objectList) {
        if (objectList == null || objectList.isEmpty()) {
            return objectList;
        }
        List<K> idList = objectList.stream().map(T::getId).collect(Collectors.toList());
        Map<K, T> objectMap = objectList.stream().collect(Collectors.toMap(T::getId, object -> object));
        List<T> objectDbList = dao.findAllById(idList);
        if (!objectDbList.isEmpty()) {
            for (T item : objectDbList) {
                ((PriorityObject) item).setPriority(((PriorityObject) objectMap.get(item.getId())).getPriority());
            }
            dao.saveAll(objectDbList);
        }
        return objectDbList;
    }
}

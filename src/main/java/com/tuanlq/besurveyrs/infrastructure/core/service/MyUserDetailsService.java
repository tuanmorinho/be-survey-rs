package com.tuanlq.besurveyrs.infrastructure.core.service;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.app.bo.Role;
import com.tuanlq.besurveyrs.app.dao.AccountDao;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final AccountDao accountDao;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Account dbAccountBo = accountDao.findByUsernameWithRoles(username)
                .orElseThrow(() ->
                        new MyException("error.unauthorized"));

        Set<Role> authorities = dbAccountBo.getRoles();

        Account account = new Account();
        account.setId(dbAccountBo.getId());
        account.setMobile(dbAccountBo.getMobile());
        account.setEmail(dbAccountBo.getEmail());
        account.setUsername(dbAccountBo.getUsername());
        account.setFullName(dbAccountBo.getFullName());
        account.setPassword(dbAccountBo.getPassword());
        account.setRoles(authorities);

        return account;
    }
}

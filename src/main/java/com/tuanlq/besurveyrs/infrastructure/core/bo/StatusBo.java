package com.tuanlq.besurveyrs.infrastructure.core.bo;

public interface StatusBo extends Bo<Integer> {
    public static final String _isTrash = "isTrash";
    public static final String _status = "status";

    Boolean getIsTrash();

    Integer getStatus();
}

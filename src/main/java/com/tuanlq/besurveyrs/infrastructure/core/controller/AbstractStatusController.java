package com.tuanlq.besurveyrs.infrastructure.core.controller;

import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.obj.StatusInfo;
import com.tuanlq.besurveyrs.infrastructure.core.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class AbstractStatusController<T extends StatusBo> extends AbstractController<T> {

    @Autowired
    protected StatusService<T> statusService;

    @PostMapping("/trash/{id}")
    public T trash(@PathVariable Integer id, @RequestBody T bo) {
        ((AbstractBo) bo).setId(id);
        return statusService.doTrash(bo);
    }

    @PostMapping({"/restore/{id}"})
    public T restore(@PathVariable Integer id, @RequestBody T bo) {
        ((AbstractBo)bo).setId(id);
        return statusService.doRestore(bo);
    }

    @PostMapping({"/updateStatus/{id}"})
    public T updateStatus(@PathVariable Integer id, @RequestBody T bo) {
        ((AbstractBo)bo).setId(id);
        return statusService.doUpdateStatus(bo);
    }

    @GetMapping({"/statusInfo"})
    public StatusInfo statusInfo() {
        return statusService.statusInfo();
    }
}

package com.tuanlq.besurveyrs.infrastructure.core.utils;

import com.tuanlq.besurveyrs.infrastructure.exception.MyException;

import java.io.*;

public class BeanUtils extends org.springframework.beans.BeanUtils {

    public static <T extends Serializable> T clone(T x) {
        if (x == null) {
            return null;
        }
        try {
            return cloneX(x);
        } catch (Exception e) {
            throw new MyException("error.object.clone");
        }
    }

    public static boolean nullSafeEquals(Object o1, Object o2) {
        if (o1 == null || o2 == null) {
            return false;
        }
        if (o1 == o2) {
            return true;
        }
        return o1.equals(o2);
    }

    private static <T extends Serializable> T cloneX(T x) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        new ObjectOutputStream(bout).writeObject(x);
        return (T) new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray())).readObject();
    }
}

package com.tuanlq.besurveyrs.infrastructure.core.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPage {
    private Integer page = 1;
    private Integer take = 10;

    private String sort;

    public MyPage() {
    }
}

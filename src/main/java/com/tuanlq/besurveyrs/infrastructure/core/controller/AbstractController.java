package com.tuanlq.besurveyrs.infrastructure.core.controller;

import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;
import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public class AbstractController<T extends Bo<Integer>> {

    @Autowired
    protected Service<T, Integer> service;

    @PostMapping("/find")
    public List<T> find(@RequestBody @Nullable T bo) {
        return service.find(bo);
    }

    @GetMapping("/get/{id}")
    public T get(@PathVariable Integer id) {
        return service.get(id);
    }

    @PostMapping("/insert")
    public T insert(@RequestBody T bo) {
        return service.doInsert(bo);
    }

    @PostMapping("/update/{id}")
    public T update(@PathVariable Integer id, @RequestBody T bo) {
        ((AbstractBo) bo).setId(id);
        return service.doUpdate(bo);
    }

    @PostMapping("/delete/{id}")
    public T delete(@PathVariable Integer id, @RequestBody T bo) {
        ((AbstractBo) bo).setId(id);
        return service.doDelete(bo);
    }
}

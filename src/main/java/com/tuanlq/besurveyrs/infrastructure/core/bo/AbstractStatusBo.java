package com.tuanlq.besurveyrs.infrastructure.core.bo;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@MappedSuperclass
public abstract class AbstractStatusBo extends AbstractBo implements StatusBo {
    @Column(name = "IS_TRASH")
    protected Boolean isTrash;
    @Column(name = "STATUS")
    protected Integer status;

    @Override
    public void copyData(Object object) {
        AbstractStatusBo bo = (AbstractStatusBo) object;
        super.copyData(bo);
        isTrash = bo.isTrash;
        status = bo.status;
    }
}
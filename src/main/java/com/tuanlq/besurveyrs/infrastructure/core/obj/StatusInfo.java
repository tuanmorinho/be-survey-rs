package com.tuanlq.besurveyrs.infrastructure.core.obj;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@RequiredArgsConstructor
public class StatusInfo {
    private List<Map<String, ?>> statusCount;
    private List<Map<String, ?>> trashCount;

    public StatusInfo(List<Map<String, ?>> statusCount, List<Map<String, ?>> trashCount) {
        this.statusCount = statusCount;
        this.trashCount = trashCount;
    }
}

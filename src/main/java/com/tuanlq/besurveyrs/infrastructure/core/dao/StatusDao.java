package com.tuanlq.besurveyrs.infrastructure.core.dao;

import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface StatusDao<T extends StatusBo> extends Dao<T, Integer> {
}
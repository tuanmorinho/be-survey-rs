package com.tuanlq.besurveyrs.infrastructure.core.dao;

import com.tuanlq.besurveyrs.infrastructure.core.bo.ObjectCategory;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjectCategoryDao extends Dao<ObjectCategory, Integer> {
}

package com.tuanlq.besurveyrs.infrastructure.core.utils;

import java.util.Random;

public class StringUtils {

    public static final String NUMBER_CODE = "0123456789";
    public static final String CHAR_CODE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" + NUMBER_CODE;

    public static String getRandomCode(int length) {
        return getRandomCode(length, CHAR_CODE);
    }

    public static String getRandomCode(int length, String s) {
        return getRandomCode(length, s.toCharArray());
    }

    public static String getRandomCode(int length, char... chars) {
        StringBuilder build = new StringBuilder(length);
        Random source = new Random();
        for (int i = 0; i < length; i++) {
            build.append(chars[source.nextInt(chars.length)]);
        }
        return build.toString();
    }

    public static String generateSlug(String name) {
        String slug = name.toLowerCase().replaceAll("[^a-z0-9\\s]", "").replaceAll("\\s+", "-");
        return slug;
    }
}

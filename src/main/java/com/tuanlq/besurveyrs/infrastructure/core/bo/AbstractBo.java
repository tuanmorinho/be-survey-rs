package com.tuanlq.besurveyrs.infrastructure.core.bo;

import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.Instant;

@Getter
@Setter
@RequiredArgsConstructor
@MappedSuperclass
public abstract class AbstractBo implements Bo<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    protected Integer id;

    @Column(name = "CREATED_BY")
    protected String createdBy;

    @CreationTimestamp
    @Column(name = "CREATED_DATE", nullable = false, updatable = false)
    protected Instant createdDate;

    @Column(name = "MODIFIED_BY")
    protected String modifiedBy;

    @UpdateTimestamp
    @Column(name = "MODIFIED_DATE", nullable = false)
    protected Instant modifiedDate;

    @PrePersist
    public void beforeInsert() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            createdBy = auth.getName();
        }
    }

    @PreUpdate
    public void beforeUpdate() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            modifiedBy = auth.getName();
        }
    }

    @Override
    public boolean equalsKey(Object object) {
        if (!(object instanceof AbstractBo key)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        return BeanUtils.nullSafeEquals(id, key.id);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        return this.equalsKey(object) && this.equalsData(object);
    }

    @Override
    public void copyData(Object object) {
        AbstractBo bo = (AbstractBo) object;
        id = bo.id;
        modifiedBy = bo.modifiedBy;
        modifiedDate = bo.modifiedDate;
    }
}

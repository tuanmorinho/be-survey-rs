package com.tuanlq.besurveyrs.infrastructure.core.dao;

import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@NoRepositoryBean
public interface Dao<T extends Bo<K>, K extends Serializable> extends JpaRepository<T, K>, JpaSpecificationExecutor<T> {
}

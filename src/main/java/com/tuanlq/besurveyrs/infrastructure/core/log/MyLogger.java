package com.tuanlq.besurveyrs.infrastructure.core.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyLogger {

    private static Logger getLogger() {
        StackTraceElement[] stackTraceElements = new Throwable().getStackTrace();
        String category = stackTraceElements[2].getClassName();
        return LoggerFactory.getLogger(category);
    }

    public static void debug(String s) {
        getLogger().debug(s);
    }

    public static void info(String s) {
        getLogger().info(s);
    }

    public static void warn(String s) {
        getLogger().warn(s);
    }

    public static void warn(Throwable t) {
        getLogger().warn(t.getLocalizedMessage(), t);
    }

    public static void error(String s) {
        getLogger().error(s);
    }

    public static void error(Throwable t) {
        getLogger().error(t.getLocalizedMessage(), t);
    }
}

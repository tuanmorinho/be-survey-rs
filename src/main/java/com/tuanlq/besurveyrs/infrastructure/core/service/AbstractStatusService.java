package com.tuanlq.besurveyrs.infrastructure.core.service;

import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractStatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.obj.StatusInfo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractStatusService<T extends StatusBo> extends AbstractService<T, Integer> implements StatusService<T> {

    @Override
    public T doTrash(T bo) {
        T dbBo = get(bo.getId());
        ((AbstractStatusBo) dbBo).setIsTrash(Boolean.TRUE);
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public T doRestore(T bo) {
        T dbBo = get(bo.getId());
        ((AbstractStatusBo) dbBo).setIsTrash(Boolean.FALSE);
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public T doUpdateStatus(T bo) {
        T dbBo = get(bo.getId());
        ((AbstractStatusBo) dbBo).setStatus(bo.getStatus());
        dao.save(dbBo);
        return dbBo;
    }
}

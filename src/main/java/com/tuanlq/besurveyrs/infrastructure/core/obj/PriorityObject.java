package com.tuanlq.besurveyrs.infrastructure.core.obj;

public interface PriorityObject {
    void setPriority(Integer priority);

    Integer getPriority();
}

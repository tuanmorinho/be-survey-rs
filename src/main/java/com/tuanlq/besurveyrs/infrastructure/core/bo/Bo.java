package com.tuanlq.besurveyrs.infrastructure.core.bo;

import java.io.Serializable;
import java.time.Instant;

public interface Bo<T extends Serializable> extends Serializable {
    public static final String _id = "id";
    public static final String _createdBy = "createdBy";
    public static final String _createdDate = "createdDate";
    public static final String _modifiedBy = "modifiedBy";
    public static final String _modifiedDate = "modifiedDate";

    T getId();

    String getCreatedBy();
    
    Instant getCreatedDate();

    String getModifiedBy();

    Instant getModifiedDate();

    boolean equalsKey(Object object);

    boolean equalsData(Object object);

    void copyData(Object object);
}

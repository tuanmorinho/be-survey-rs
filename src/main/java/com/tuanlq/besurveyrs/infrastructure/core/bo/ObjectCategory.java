package com.tuanlq.besurveyrs.infrastructure.core.bo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_OBJECT_CATEGORY")
public class ObjectCategory extends AbstractBo implements Bo<Integer> {

    @Column(name = "TYPE")
    private String type;

    @Column(name = "PROPERTY")
    private String property;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "LABEL")
    private String label;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @Column(name = "VALUE")
    private Integer value;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Override
    public boolean equalsData(Object object) {
        return false;
    }
}

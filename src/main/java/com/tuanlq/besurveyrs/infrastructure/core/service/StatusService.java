package com.tuanlq.besurveyrs.infrastructure.core.service;

import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.obj.StatusInfo;

public interface StatusService<T extends StatusBo> extends Service<T, Integer> {
    T doTrash(T bo);

    T doRestore(T bo);

    T doUpdateStatus(T bo);

    StatusInfo statusInfo();
}

package com.tuanlq.besurveyrs.infrastructure.init;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.app.bo.Role;
import com.tuanlq.besurveyrs.app.common.constant.AccountConstant;
import com.tuanlq.besurveyrs.app.common.enumerate.RoleEnum;
import com.tuanlq.besurveyrs.app.dao.AccountDao;
import com.tuanlq.besurveyrs.app.dao.RoleDao;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class InitDB {

    private final RoleDao roleDao;
    private final AccountDao accountDao;
    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    private void initData() {
        List<Role> roleListDb = roleDao.findAll();
        List<RoleEnum> roleEnums = Arrays.asList(RoleEnum.values());
        if (!roleListDb.isEmpty()) {
            List<String> roleNameList = roleListDb.stream().map(Role::getName).toList();
            List<String> roleEnumNameList = roleEnums.stream().map(RoleEnum::name).toList();
            if (!roleEnumNameList.equals(roleNameList)) {
                roleDao.deleteAll();
            } else return;
        }
        roleDao.deleteAll();
        List<Role> roles = roleEnums.stream()
                .map(roleEnum -> {
                    Role role = new Role();
                    role.setName(roleEnum.name());
                    role.setDescription("Phân quyền " + roleEnum.name().toLowerCase());
                    return role;
                }).collect(Collectors.toList());
        roleDao.saveAll(roles);


        Account accountDb = accountDao.findByUsernameWithRoles("admin@gmail.com").orElse(null);
        if (accountDb != null) return;
        Role roleAdmin = roleDao.getByName(RoleEnum.ADMIN.name());
        if (roleAdmin == null) return;
        accountDao.deleteAll();
        Account accountAdmin = new Account();
        accountAdmin.setRegisterType(AccountConstant.REGISTER_TYPE_ADMIN);
        accountAdmin.setUsername("admin@gmail.com");
        accountAdmin.setPassword(passwordEncoder.encode("Admin123#@"));
        accountAdmin.setFullName("Admin");
        accountAdmin.setRoles(Collections.singleton(roleAdmin));
        accountDao.save(accountAdmin);
    }
}
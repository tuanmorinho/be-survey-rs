package com.tuanlq.besurveyrs.infrastructure.config;

import com.tuanlq.besurveyrs.infrastructure.filter.AuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final AuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .cors(Customizer.withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        /*
                         * For Trigger Job
                         * */
                        .requestMatchers("/job/**").permitAll()

                        /*
                         * For End-User
                         * */
                        .requestMatchers("/v2/**").permitAll()

                        /*
                        * For Admin
                        * */
                        .requestMatchers("/auth/**").permitAll()
                        .requestMatchers("/account/find").hasAnyAuthority("ACCOUNT", "ADMIN", "ROOT")
                        .requestMatchers("/account/**").hasAnyAuthority("ACCOUNT", "ADMIN", "ROOT")
                        .requestMatchers("/survey-category/find").hasAnyAuthority("SURVEY", "ADMIN", "ROOT")
                        .requestMatchers("/survey-category/**").hasAnyAuthority("SURVEY", "ADMIN", "ROOT")
                        .requestMatchers("/survey/find").hasAnyAuthority("SURVEY", "ADMIN", "ROOT")
                        .requestMatchers("/survey/**").hasAnyAuthority("SURVEY", "ADMIN", "ROOT")
                        .requestMatchers("/survey-question/find").hasAnyAuthority("SURVEY", "ADMIN", "ROOT")
                        .requestMatchers("/survey-question/**").hasAnyAuthority("SURVEY", "ADMIN", "ROOT")
                        .requestMatchers("/file/**").hasAnyAuthority("FILE", "ADMIN", "ROOT")
                        .requestMatchers("/folder/**").hasAnyAuthority("FILE", "ADMIN", "ROOT")

                        /*
                        * remaining
                        * */
                        .anyRequest().authenticated()
                )
                .sessionManagement(session -> session
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return httpSecurity.build();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("http://localhost:4201", "http://localhost:4200"));
        configuration.setAllowedMethods(List.of("GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS", "HEAD"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(List.of("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}

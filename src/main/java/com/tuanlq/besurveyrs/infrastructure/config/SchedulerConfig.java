package com.tuanlq.besurveyrs.infrastructure.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@ConditionalOnProperty(name = "spring.quartz.auto-startup", havingValue = "true")
public class SchedulerConfig {
}

package com.tuanlq.besurveyrs.infrastructure.utils;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;

public class CookieUtil {
    public static final String COOKIE_VISITOR_UUID = "visitor-uuid";

    public CookieUtil() {
    }

    public static String getCookieValue(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            Cookie[] var3 = cookies;
            int var4 = cookies.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Cookie cookie = var3[var5];
                if (cookie.getName().equalsIgnoreCase(cookieName)) {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }

    public static String getCookieVisitorUuid(HttpServletRequest request) {
        return getCookieValue(request, COOKIE_VISITOR_UUID);
    }
}

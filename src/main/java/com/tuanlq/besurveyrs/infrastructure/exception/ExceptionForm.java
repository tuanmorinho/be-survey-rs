package com.tuanlq.besurveyrs.infrastructure.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ExceptionForm {
    private HttpStatus status;
    private String message;

    public ExceptionForm(HttpStatus status, String message) {
        super();
        this.status = status;
        this.message = message;
    }
}

package com.tuanlq.besurveyrs.infrastructure.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
@RequiredArgsConstructor
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(MyException.class)
    public ResponseEntity<Object> handlerServerApiException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        return handleMyException((MyException) ex, headers, status, request);
    }

    private ResponseEntity<Object> handleMyException(MyException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionForm exceptionForm = new ExceptionForm(status, messageSource.getMessage(ex.getMessage(), ex.getArgs(), Locale.of("vi")));
        return handleExceptionInternal(ex, exceptionForm, headers, status, request);
    }

}

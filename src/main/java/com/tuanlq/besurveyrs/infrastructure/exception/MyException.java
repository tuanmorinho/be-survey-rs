package com.tuanlq.besurveyrs.infrastructure.exception;

public class MyException extends RuntimeException {
    private Object[] args;

    public MyException(String message) {
        super(message);
    }

    public MyException(String message, Object[] args) {
        super(message);
        this.args = args;
    }

    public Object[] getArgs() {
        return args;
    }
}

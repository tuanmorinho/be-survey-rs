package com.tuanlq.besurveyrs;

import com.tuanlq.besurveyrs.infrastructure.config.SchedulerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import({SchedulerConfig.class})
@SpringBootApplication
public class BeSurveyRsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeSurveyRsApplication.class, args);
    }

}

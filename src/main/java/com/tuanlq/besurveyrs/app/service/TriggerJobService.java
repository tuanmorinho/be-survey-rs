package com.tuanlq.besurveyrs.app.service;

import org.quartz.SchedulerException;

public interface TriggerJobService {

    void triggerCountSurveyActiveJob() throws SchedulerException;

    void triggerCountSurveyTotalJob() throws SchedulerException;

    void triggerCountSurveyQuestionJob() throws SchedulerException;

    void triggerCountSurveyAnswerJob() throws SchedulerException;

}

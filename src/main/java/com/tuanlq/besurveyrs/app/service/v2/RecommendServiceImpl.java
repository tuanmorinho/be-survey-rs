package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.*;
import com.tuanlq.besurveyrs.app.common.constant.ProductContanst;
import com.tuanlq.besurveyrs.app.dao.*;
import com.tuanlq.besurveyrs.app.facade.AuthenticationFacade;
import com.tuanlq.besurveyrs.app.web.v2.recommend.FuzzyRequest;
import com.tuanlq.besurveyrs.app.web.v2.recommend.FuzzyResponse;
import com.tuanlq.besurveyrs.app.web.v2.recommend.RecommendRequest;
import com.tuanlq.besurveyrs.app.web.v2.recommend.RecommendResponse;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.utils.CookieUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RecommendServiceImpl implements RecommendService {

    private final SurveyAccountDao surveyAccountDao;
    private final SurveyAccountQuestionDao surveyAccountQuestionDao;
    private final SurveyAccountAnswerDao surveyAccountAnswerDao;
    private final SurveyDao surveyDao;
    private final ProductDao productDao;
    private final AuthenticationFacade authenticationFacade;
    private final RestTemplate restTemplate;

    @Override
    public RecommendResponse getResult(HttpServletRequest request, RecommendRequest model) {
        if (model == null) {
            throw new MyException("error.recommend");
        }
        if (model.getSurveyId() == null) {
            throw new MyException("error.recommend");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("error.recommend");
        }

        FuzzyRequest fuzzyRequest = new FuzzyRequest();

        List<Product> productList = productDao.findAll((root, query, builder) -> builder.and(
           builder.equal(root.get(Product._surveyCategory).get(SurveyCategory._id), surveyDb.getSurveyCategory().getId()),
                builder.equal(root.get(Product._isTrash), Boolean.FALSE),
                builder.equal(root.get(Product._status), ProductContanst.STATUS_ACTIVE)
        ));
        if (productList.isEmpty()) {
            throw new MyException("error.recommend");
        }
        fuzzyRequest.setProductList(productList);

        String visitorUuid = CookieUtil.getCookieVisitorUuid(request);
        if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
            Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
            if (account == null) {
                throw new MyException("error.recommend");
            }
            SurveyAccount surveyAccountByAccount = surveyAccountDao.findBySurveyIdAndAccountId(surveyDb.getId(), account.getId());
            List<SurveyAccountQuestion> surveyAccountQuestionList = surveyAccountQuestionDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountByAccount.getId())
            ));
            List<SurveyAccountAnswer> surveyAccountAnswerList = new ArrayList<>();
            for (SurveyAccountQuestion surveyAccountQuestion : surveyAccountQuestionList) {
                List<SurveyAccountAnswer> surveyAccountAnswerBySurveyAccountQuestionList = surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
                        builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), surveyAccountQuestion.getId()),
                        builder.equal(root.get(SurveyAccountAnswer._account).get(Account._id), account.getId())
                ));
                if (!surveyAccountAnswerBySurveyAccountQuestionList.isEmpty()) {
                    surveyAccountAnswerList.addAll(surveyAccountAnswerBySurveyAccountQuestionList);
                }
            }
            double demandScore = surveyAccountAnswerList.stream().mapToDouble(SurveyAccountAnswer::getAnswerDemandScore).sum() / surveyAccountAnswerList.size();
            fuzzyRequest.setDemandScore(demandScore);
        } else {
            if (visitorUuid != null) {
                SurveyAccount surveyAccountByVisitorUuid = surveyAccountDao.findBySurveyIdAndVisitorUuidAndAccountIsNull(surveyDb.getId(), visitorUuid);
                List<SurveyAccountQuestion> surveyAccountQuestionList = surveyAccountQuestionDao.findAll((root, query, builder) -> builder.and(
                        builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountByVisitorUuid.getId())
                ));
                List<SurveyAccountAnswer> surveyAccountAnswerList = new ArrayList<>();
                for (SurveyAccountQuestion surveyAccountQuestion : surveyAccountQuestionList) {
                    List<SurveyAccountAnswer> surveyAccountAnswerBySurveyAccountQuestionList = surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
                            builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), surveyAccountQuestion.getId()),
                            builder.isNull(root.get(SurveyAccountAnswer._account).get(Account._id))
                    ));
                    if (!surveyAccountAnswerBySurveyAccountQuestionList.isEmpty()) {
                        surveyAccountAnswerList.addAll(surveyAccountAnswerBySurveyAccountQuestionList);
                    }
                }
                double demandScore = surveyAccountAnswerList.stream().mapToDouble(SurveyAccountAnswer::getAnswerDemandScore).sum() / surveyAccountAnswerList.size();
                fuzzyRequest.setDemandScore(demandScore);
            }
        }

        FuzzyResponse fuzzyResponse = fuzzySimulation(fuzzyRequest);

        Product productResult = productDao.findById(fuzzyResponse.getProduct().getId()).orElse(null);
        if (productResult == null) {
            throw new MyException("error.recommend");
        }

        RecommendResponse recommendResponse = new RecommendResponse();
        recommendResponse.setProduct(productResult);
        return recommendResponse;
    }

    private FuzzyResponse fuzzySimulation(FuzzyRequest model) {
        String url = "http://localhost:8001/api/fuzzyProcess/calculate";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<FuzzyRequest> requestEntity = new HttpEntity<>(model, headers);
        return restTemplate.postForObject(url, requestEntity, FuzzyResponse.class);
    }
}

package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import com.tuanlq.besurveyrs.app.web.surveyquestion.SurveyQuestionRequest;
import com.tuanlq.besurveyrs.app.web.surveyquestion.SurveyQuestionResponse;
import com.tuanlq.besurveyrs.app.web.surveyquestion.SurveyQuestionSearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SurveyQuestionService extends Service<SurveyQuestion, Integer> {

    Page<SurveyQuestion> findBySearch(SurveyQuestionSearchRequest model);

    SurveyQuestionResponse getSurveyQuestionDetail(Integer id);

    SurveyQuestion doCreateSurveyQuestion(SurveyQuestionRequest model);

    SurveyQuestion doUpdateSurveyQuestion(SurveyQuestionRequest model);

    SurveyQuestion doDeleteSurveyQuestion(SurveyQuestionRequest model);

    boolean validateCreateSurveyQuestion(SurveyQuestionRequest model);

}

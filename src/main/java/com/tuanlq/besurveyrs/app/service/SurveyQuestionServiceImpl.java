package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.*;
import com.tuanlq.besurveyrs.app.common.constant.SurveyConstant;
import com.tuanlq.besurveyrs.app.dao.SurveyAnswerDao;
import com.tuanlq.besurveyrs.app.dao.SurveyDao;
import com.tuanlq.besurveyrs.app.dao.SurveyQuestionDao;
import com.tuanlq.besurveyrs.app.dao.SurveyQuestionStepDao;
import com.tuanlq.besurveyrs.app.web.surveyquestion.SurveyQuestionRequest;
import com.tuanlq.besurveyrs.app.web.surveyquestion.SurveyQuestionResponse;
import com.tuanlq.besurveyrs.app.web.surveyquestion.SurveyQuestionSearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractService;
import com.tuanlq.besurveyrs.infrastructure.core.utils.PageUtil;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SurveyQuestionServiceImpl extends AbstractService<SurveyQuestion, Integer> implements SurveyQuestionService {

    private final SurveyQuestionDao surveyQuestionDao;
    private final SurveyAnswerDao surveyAnswerDao;
    private final SurveyDao surveyDao;
    private final SurveyQuestionStepDao surveyQuestionStepDao;

    @Override
    public Page<SurveyQuestion> findBySearch(SurveyQuestionSearchRequest model) {
        if (model.getSurveyId() == null) {
            throw new MyException("survey.answer.surveyId.required");
        }

        Pageable pageable = PageUtil.getPageRequest(model);
        return surveyQuestionDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if (model.getSurveyId() != null) {
                predicates.add(builder.equal(root.get(SurveyQuestion._survey).get(Survey._id), model.getSurveyId()));
            }

            query.orderBy(builder.desc(root.get(SurveyQuestion._createdDate)), builder.desc(root.get(SurveyQuestion._id)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public SurveyQuestionResponse getSurveyQuestionDetail(Integer id) {
        if (id == null) {
            throw new MyException("error.get");
        }

        SurveyQuestion dbBo = surveyQuestionDao.findById(id).orElse(null);
        if (dbBo == null) {
            throw new MyException("error.get");
        }

        SurveyQuestionResponse surveyQuestionResponse = new SurveyQuestionResponse();
        if (dbBo.getSurvey() != null) {
            surveyQuestionResponse.setSurveyId(dbBo.getSurvey().getId());
        }
        surveyQuestionResponse.setId(dbBo.getId());
        surveyQuestionResponse.setName(dbBo.getName());
        surveyQuestionResponse.setDescription(dbBo.getDescription());
        surveyQuestionResponse.setImageThumbnail(dbBo.getImageThumbnail());
        surveyQuestionResponse.setImageFeature(dbBo.getImageFeature());
        surveyQuestionResponse.setAnswerDisplayType(dbBo.getAnswerDisplayType());
        surveyQuestionResponse.setAnswerSelectType(dbBo.getAnswerSelectType());
        surveyQuestionResponse.setIsAnswerRequired(dbBo.getIsAnswerRequired());
        surveyQuestionResponse.setIsFirst(dbBo.getIsFirst());

        return surveyQuestionResponse;
    }

    @Override
    public SurveyQuestion doCreateSurveyQuestion(SurveyQuestionRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getName() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getSurveyId() == null) {
            throw new MyException("action.invalid");
        }

        Survey dbSurveyBo = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (dbSurveyBo == null) {
            throw new MyException("action.invalid");
        }

        boolean isAlreadyHadFirstQuestion = checkAlreadyHadFirstQuestion(model);
        if (isAlreadyHadFirstQuestion) {
            throw new MyException("survey.question.survey.question.is.first");
        }

        SurveyQuestion surveyQuestion = new SurveyQuestion();
        surveyQuestion.setSurvey(dbSurveyBo);
        surveyQuestion.setName(model.getName());
        surveyQuestion.setDescription(model.getDescription());
        surveyQuestion.setImageThumbnail(model.getImageThumbnail());
        surveyQuestion.setImageFeature(model.getImageFeature());
        surveyQuestion.setAnswerDisplayType(model.getAnswerDisplayType());
        surveyQuestion.setAnswerSelectType(model.getAnswerSelectType());
        surveyQuestion.setIsAnswerRequired(model.getIsAnswerRequired());
        surveyQuestion.setIsFirst(model.getIsFirst() ? Boolean.TRUE : Boolean.FALSE);

        SurveyQuestion surveyQuestionSaved = surveyQuestionDao.save(surveyQuestion);
        if (Boolean.TRUE.equals(surveyQuestionSaved.getIsFirst())) {
            SurveyQuestionStep surveyQuestionStep = new SurveyQuestionStep();
            surveyQuestionStep.setSurveyQuestion(surveyQuestionSaved);
            surveyQuestionStepDao.save(surveyQuestionStep);
        }

        return surveyQuestion;
    }

    @Override
    public SurveyQuestion doUpdateSurveyQuestion(SurveyQuestionRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getId() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getName() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getSurveyId() == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion dbBo = surveyQuestionDao.findById(model.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }

        Survey dbSurveyBo = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (dbSurveyBo == null) {
            throw new MyException("action.invalid");
        }

        dbBo.setName(model.getName());
        dbBo.setDescription(model.getDescription());
        dbBo.setImageThumbnail(model.getImageThumbnail());
        dbBo.setImageFeature(model.getImageFeature());
        dbBo.setAnswerDisplayType(model.getAnswerDisplayType());
        dbBo.setAnswerSelectType(model.getAnswerSelectType());
        dbBo.setIsAnswerRequired(model.getIsAnswerRequired());

        /*
         * If question is next question of another question, cannot update isFirst
         * */
        boolean isSurveyQuestionNext = checkSurveyQuestionIsNextQuestion(model);
        if (!isSurveyQuestionNext) {
            dbBo.setIsFirst(model.getIsFirst());
        }

        surveyQuestionDao.save(dbBo);

        return dbBo;
    }

    @Override
    public SurveyQuestion doDeleteSurveyQuestion(SurveyQuestionRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getId() == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion dbBo = surveyQuestionDao.findById(model.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }

        List<SurveyAnswer> surveyAnswerListBySurveyQuestionId = surveyAnswerDao.findAllBySurveyQuestionId(dbBo.getId());
        surveyAnswerDao.deleteAll(surveyAnswerListBySurveyQuestionId);

        surveyQuestionDao.delete(dbBo);

        return dbBo;
    }

    @Override
    public boolean validateCreateSurveyQuestion(SurveyQuestionRequest model) {
        if (model == null) {
            return false;
        }

        if (model.getSurveyId() == null) {
            return false;
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            return false;
        }
        if (surveyDb.getStatus().equals(SurveyConstant.STATUS_ACTIVE) || surveyDb.getIsTrash().equals(Boolean.TRUE)) {
            return false;
        }

        return true;
    }

    private boolean checkAlreadyHadFirstQuestion(SurveyQuestionRequest model) {
        SurveyQuestion surveyQuestion = surveyQuestionDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(SurveyQuestion._survey).get(Survey._id), model.getSurveyId()));
            predicates.add(builder.equal(root.get(SurveyQuestion._isFirst), model.getIsFirst()));
            return builder.and(predicates.toArray(new Predicate[0]));
        }).stream().findFirst().orElse(null);
        return surveyQuestion != null;
    }

    private boolean checkSurveyQuestionIsNextQuestion(SurveyQuestionRequest model) {
        SurveyQuestionStep surveyQuestionStep = surveyQuestionStepDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(SurveyQuestionStep._surveyQuestionNext).get(SurveyQuestion._id), model.getId()));
            return builder.and(predicates.toArray(new Predicate[0]));
        }).stream().findFirst().orElse(null);
        return surveyQuestionStep != null;
    }
}

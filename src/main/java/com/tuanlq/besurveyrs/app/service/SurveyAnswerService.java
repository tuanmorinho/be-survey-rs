package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.web.surveyanswer.SurveyAnswerRequest;
import com.tuanlq.besurveyrs.app.web.surveyanswer.SurveyAnswerResponse;
import com.tuanlq.besurveyrs.app.web.surveyanswer.SurveyAnswerSearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SurveyAnswerService extends Service<SurveyAnswer, Integer> {

    Page<SurveyAnswer> findBySearch(SurveyAnswerSearchRequest model);

    List<SurveyAnswer> findAllSurveyAnswer(Integer surveyQuestionId);

    SurveyAnswerResponse getSurveyAnswerDetail(Integer id);

    SurveyAnswer doCreateSurveyAnswer(SurveyAnswerRequest model);

    SurveyAnswer doUpdateSurveyAnswer(SurveyAnswerRequest model);

    boolean validateCreateSurveyAnswer(SurveyAnswerRequest model);
}

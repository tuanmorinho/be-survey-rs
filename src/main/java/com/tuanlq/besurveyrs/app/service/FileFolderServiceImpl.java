package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.app.bo.FileStorage;
import com.tuanlq.besurveyrs.app.dao.FileFolderDao;
import com.tuanlq.besurveyrs.app.dao.FileStorageDao;
import com.tuanlq.besurveyrs.app.web.file.FileResponse;
import com.tuanlq.besurveyrs.app.web.folder.FileFolderListResponse;
import com.tuanlq.besurveyrs.app.web.folder.FileFolderRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractService;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileFolderServiceImpl extends AbstractService<FileFolder, Integer> implements FileFolderService {

    private final FileFolderDao fileFolderDao;
    private final FileStorageDao fileStorageDao;

    private List<FileFolder> folders = new ArrayList<>();

    @Override
    public FileFolderListResponse viewFolder(FileFolder bo) {
        List<FileFolder> folderList;
        List<FileResponse> fileList;

        if (bo.getId() == null) {
            folderList = fileFolderDao.findAll((root, query, builder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.isNull(root.get(FileFolder._fileFolder)));
                query.orderBy(builder.desc(root.get(FileFolder._createdDate)));
                return builder.and(predicates.toArray(new Predicate[0]));
            });

            fileList = fileStorageDao.findFileNotInFolder();

            FileFolderListResponse fileFolderResponse = new FileFolderListResponse();
            fileFolderResponse.setFolderList(folderList);
            fileFolderResponse.setFileList(fileList);
            return fileFolderResponse;
        } else {
            folderList = fileFolderDao.findAll((root, query, builder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get(FileFolder._fileFolder).get(FileFolder._id), bo.getId()));
                query.orderBy(builder.desc(root.get(FileFolder._createdDate)));
                return builder.and(predicates.toArray(new Predicate[0]));
            });

            FileFolder fileFolder = fileFolderDao.findById(bo.getId()).orElse(null);
            if (fileFolder == null) {
                throw new MyException("action.invalid");
            }
            fileList = fileStorageDao.findFileInFolder(fileFolder);

            FileFolderListResponse fileFolderResponse = new FileFolderListResponse();
            fileFolderResponse.setFolderList(folderList);
            fileFolderResponse.setFileList(fileList);
            return fileFolderResponse;
        }
    }

    @Override
    public FileFolder doInsertFolder(FileFolderRequest model) {
        if (model.getName() == null) {
            throw new MyException("action.invalid");
        }

        FileFolder fileFolder = new FileFolder();
        fileFolder.setName(model.getName());
        if (model.getFileFolderParentId() != null) {
            FileFolder folderParent = fileFolderDao.findById(model.getFileFolderParentId()).orElse(null);
            if (folderParent == null) {
                throw new MyException("action.invalid");
            }
            fileFolder.setFileFolder(folderParent);
        }

        return fileFolderDao.save(fileFolder);
    }

    @Override
    public FileFolder doDelete(FileFolder bo) {
        if (bo == null) {
            throw new MyException("action.invalid");
        }

        List<FileFolder> folderList = getAllChildFolderRecursion(bo);
        Collections.reverse(folderList);
        for (FileFolder folder : folderList) {
            List<FileStorage> fileStorageList = fileStorageDao.findAll((root, query, builder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get(FileStorage._fileFolder).get(FileFolder._id), folder.getId()));
                return builder.and(predicates.toArray(new Predicate[0]));
            });
            if (fileStorageList.size() > 0) {
                fileStorageDao.deleteAll(fileStorageList);
            }
            fileFolderDao.delete(folder);
        }

        return bo;
    }

    private List<FileFolder> getAllChildFolderRecursion(FileFolder folder) {
        List<FileFolder> resultList = new ArrayList<>();
        if (folder != null) {
            resultList.add(folder);

            List<FileFolder> childFolderList = fileFolderDao.findAll((root, query, builder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get(FileFolder._fileFolder).get(FileFolder._id), folder.getId()));
                return builder.and(predicates.toArray(new Predicate[0]));
            });
            for (FileFolder childFolder : childFolderList) {
                resultList.addAll(getAllChildFolderRecursion(childFolder));
            }
        }
        return resultList;
    }
}

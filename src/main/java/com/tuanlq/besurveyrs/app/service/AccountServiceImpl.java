package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.app.dao.AccountDao;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl extends AbstractService<Account, Integer> implements AccountService {

    private final AccountDao accountDao;

    @Override
    public Account doInsert(Account bo) {
        return null;
    }

    @Override
    public Account doUpdate(Account bo) {
        return null;
    }

}

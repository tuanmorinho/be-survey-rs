package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.app.bo.Role;
import com.tuanlq.besurveyrs.app.common.constant.AccountConstant;
import com.tuanlq.besurveyrs.app.common.enumerate.RoleEnum;
import com.tuanlq.besurveyrs.app.dao.AccountDao;
import com.tuanlq.besurveyrs.app.dao.RoleDao;
import com.tuanlq.besurveyrs.app.web.authentication.AuthenticationRequest;
import com.tuanlq.besurveyrs.app.web.authentication.AuthenticationResponse;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.utils.JwtUtil;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AccountDao accountDao;
    private final RoleDao roleDao;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtils;
    private final AuthenticationManager authenticationManager;

    @Override
    public AuthenticationResponse register(AuthenticationRequest model) {
        if (!model.getRegisterType().equals(AccountConstant.REGISTER_TYPE_ADMIN)) {
            throw new MyException("action.invalid");
        }

        if (model.getFullName() == null) {
            throw new MyException("account.fullName.invalid");
        }
        if (model.getUsername() == null || model.getPassword() == null) {
            throw new MyException("auth.identify.invalid");
        }

        Account dbBo = findAccountByUsername(model);

        if (dbBo != null) {
            throw new MyException("error.register.duplicate_username");
        }

        Role dbRoleBo = roleDao.getByName(RoleEnum.DEFAULT.name());
        if (dbRoleBo == null) {
            throw new MyException("action.invalid");
        }

        Account account = new Account();
        account.setFullName(model.getFullName());
        account.setAvatarUrl(model.getAvatarUrl());
        account.setUsername(model.getUsername());
        account.setPassword(passwordEncoder.encode(model.getPassword()));
        account.setRegisterType(AccountConstant.REGISTER_TYPE_ADMIN);
        account.setRoles(Collections.singleton(dbRoleBo));
        accountDao.save(account);

        String token = jwtUtils.generateToken(generateClaims(account), account);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken(token);

        return authenticationResponse;
    }

    @Override
    public AuthenticationResponse login(AuthenticationRequest model) {
        if (!model.getRegisterType().equals(AccountConstant.REGISTER_TYPE_ADMIN)) {
            throw new MyException("action.invalid");
        }

        if (model.getUsername() == null || model.getPassword() == null) {
            throw new MyException("auth.identify.invalid");
        }

        Account dbBo = findAccountByUsername(model);

        if (dbBo == null) {
            throw new MyException("error.login.user_not_found");
        }

        if (!passwordEncoder.matches(model.getPassword(), dbBo.getPassword())) {
            throw new MyException("error.login.user_not_found");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        model.getUsername(),
                        model.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = jwtUtils.generateToken(generateClaims(dbBo), (UserDetails) authentication.getPrincipal());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken(token);

        return authenticationResponse;
    }

    private Account findAccountByUsername(AuthenticationRequest model) {
        return accountDao.findOne((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (model.getUsername() != null) {
                predicates.add(builder.equal(root.get(Account._username), model.getUsername()));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        }).orElse(null);
    }

    private Map<String, Map<String, String>> generateClaims(Account bo) {
        Map<String, String> accountInfoClaimMap = new HashMap<>();
        accountInfoClaimMap.put("fullName", bo.getFullName());
        accountInfoClaimMap.put("avatarUrl", bo.getAvatarUrl());
        accountInfoClaimMap.put("email", bo.getEmail());

        Map<String, Map<String, String>> extraClaims = new HashMap<>();
        extraClaims.put("account", accountInfoClaimMap);

        return extraClaims;
    }
}

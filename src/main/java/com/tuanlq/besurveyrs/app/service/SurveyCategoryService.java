package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.web.surveycategory.SurveyCategorySearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;
import org.springframework.data.domain.Page;

public interface SurveyCategoryService extends Service<SurveyCategory, Integer> {

    Page<SurveyCategory> findBySearch(SurveyCategorySearchRequest model);
}

package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Product;
import com.tuanlq.besurveyrs.infrastructure.core.service.StatusService;

public interface ProductService extends StatusService<Product> {
}

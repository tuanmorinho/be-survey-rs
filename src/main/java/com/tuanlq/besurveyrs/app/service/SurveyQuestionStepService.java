package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.SurveyQuestionStep;
import com.tuanlq.besurveyrs.app.web.surveyquestionstep.SurveyQuestionStepRequest;
import com.tuanlq.besurveyrs.app.web.surveyquestionstep.SurveyQuestionStepResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SurveyQuestionStepService {

    ResponseEntity<?> doMappingStep(List<SurveyQuestionStepRequest> model);

    List<SurveyQuestionStepResponse> getMappingDetail(Integer surveyId);
}

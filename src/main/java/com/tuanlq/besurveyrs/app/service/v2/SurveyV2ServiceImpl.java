package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.*;
import com.tuanlq.besurveyrs.app.common.constant.SurveyAccountConstant;
import com.tuanlq.besurveyrs.app.common.constant.SurveyAnswerConstant;
import com.tuanlq.besurveyrs.app.common.constant.SurveyConstant;
import com.tuanlq.besurveyrs.app.dao.*;
import com.tuanlq.besurveyrs.app.facade.AuthenticationFacade;
import com.tuanlq.besurveyrs.app.web.v2.survey.*;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.utils.CookieUtil;
import jakarta.persistence.criteria.Predicate;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SurveyV2ServiceImpl implements SurveyV2Service {

    private final SurveyDao surveyDao;
    private final SurveyQuestionDao surveyQuestionDao;
    private final SurveyAnswerDao surveyAnswerDao;
    private final SurveyQuestionStepDao surveyQuestionStepDao;
    private final SurveyAccountDao surveyAccountDao;
    private final SurveyAccountQuestionDao surveyAccountQuestionDao;
    private final SurveyAccountAnswerDao surveyAccountAnswerDao;
    private final AuthenticationFacade authenticationFacade;

    @Override
    public List<Survey> findAllSurveyByCategory(SurveyV2Request model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getSurveyCategoryId() == null) {
            throw new MyException("action.invalid");
        }

        return surveyDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(builder.equal(root.get(Survey._surveyCategory).get(SurveyCategory._id), model.getSurveyCategoryId()));
            predicates.add(builder.equal(root.get(Survey._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Survey._status), SurveyConstant.STATUS_ACTIVE));

            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    @Override
    public SurveyV2Response getSurvey(Survey bo) {
        if (bo == null) {
            throw new MyException("action.invalid");
        }

        if (bo.getId() == null) {
            throw new MyException("action.invalid");
        }

        Survey dbBo = surveyDao.findById(bo.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }

        SurveyV2Response surveyV2Response = new SurveyV2Response();
        surveyV2Response.setId(dbBo.getId());
        surveyV2Response.setSurveyCategoryId(dbBo.getSurveyCategory().getId());
        surveyV2Response.setCode(dbBo.getCode());
        surveyV2Response.setName(dbBo.getName());
        surveyV2Response.setSlug(dbBo.getSlug());
        surveyV2Response.setDescription(dbBo.getDescription());
        surveyV2Response.setImageThumbnail(dbBo.getImageThumbnail());
        surveyV2Response.setImageFeature(dbBo.getImageFeature());
        surveyV2Response.setNextStepType(dbBo.getNextStepType());

        return surveyV2Response;
    }

    @Override
    public DoSurveyResponse doSurvey(HttpServletRequest request, DoSurveyRequest model) {
        if (model.getSurveyId() == null) {
            throw new MyException("action.invalid");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("action.invalid");
        }

        DoSurveyResponse doSurveyResponse = new DoSurveyResponse();

        String visitorUuid = CookieUtil.getCookieVisitorUuid(request);

        if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
            /*
             * User is logged in
             * */
            Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
            SurveyAccount surveyAccountByAccountId = surveyAccountDao.findBySurveyIdAndAccountId(model.getSurveyId(), account.getId());

            return getDoSurveyResponse(model, surveyDb, doSurveyResponse, surveyAccountByAccountId);
        }

        if (visitorUuid != null) {
            /*
             * User is not logged in, accessing the website on different machines
             * */
            SurveyAccount surveyAccountByVisitorUuid = surveyAccountDao.findBySurveyIdAndVisitorUuidAndAccountIsNull(model.getSurveyId(), visitorUuid);

            return getDoSurveyResponse(model, surveyDb, doSurveyResponse, surveyAccountByVisitorUuid);
        }

        return null;
    }

    @Override
    public DoSurveyResponse redoSurvey(HttpServletRequest request, DoSurveyRequest model) {
        if (model.getSurveyId() == null) {
            throw new MyException("action.invalid");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("action.invalid");
        }

        DoSurveyResponse doSurveyResponse = new DoSurveyResponse();

        String visitorUuid = CookieUtil.getCookieVisitorUuid(request);
        if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
            Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
            if (account == null) {
                throw new MyException("error.recommend");
            }
            SurveyAccount surveyAccountByAccount = surveyAccountDao.findBySurveyIdAndAccountId(surveyDb.getId(), account.getId());
            List<SurveyAccountQuestion> surveyAccountQuestionListForDelete = surveyAccountQuestionDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountByAccount.getId())
            ));
            for (SurveyAccountQuestion surveyAccountQuestion : surveyAccountQuestionListForDelete) {
                List<SurveyAccountAnswer> surveyAccountAnswerListForDelete = surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
                        builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), surveyAccountQuestion.getId())
                ));
                surveyAccountAnswerDao.deleteAll(surveyAccountAnswerListForDelete);
            }
            surveyAccountQuestionDao.deleteAll(surveyAccountQuestionListForDelete);
            surveyAccountDao.delete(surveyAccountByAccount);
        } else {
            if (visitorUuid != null) {
                SurveyAccount surveyAccountByVisitorUuid = surveyAccountDao.findBySurveyIdAndVisitorUuidAndAccountIsNull(surveyDb.getId(), visitorUuid);
                List<SurveyAccountQuestion> surveyAccountQuestionListForDelete = surveyAccountQuestionDao.findAll((root, query, builder) -> builder.and(
                        builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountByVisitorUuid.getId())
                ));
                for (SurveyAccountQuestion surveyAccountQuestion : surveyAccountQuestionListForDelete) {
                    List<SurveyAccountAnswer> surveyAccountAnswerListForDelete = surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
                            builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), surveyAccountQuestion.getId())
                    ));
                    surveyAccountAnswerDao.deleteAll(surveyAccountAnswerListForDelete);
                }
                surveyAccountQuestionDao.deleteAll(surveyAccountQuestionListForDelete);
                surveyAccountDao.delete(surveyAccountByVisitorUuid);
            }
        }
        return getDoSurveyResponse(model, surveyDb, doSurveyResponse, null);
    }


    @Override
    public DoSurveyResponse submitAnswer(HttpServletRequest request, SubmitSurveyAnswerRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getQuestionId() == null) {
            throw new MyException("action.invalid");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion surveyQuestionDb = surveyQuestionDao.findById(model.getQuestionId()).orElse(null);
        if (surveyQuestionDb == null) {
            throw new MyException("action.invalid");
        }
        validateQuestionRequireAnswer(surveyQuestionDb, model);

        DoSurveyResponse doSurveyResponse = new DoSurveyResponse();

        SurveyAccountQuestion surveyAccountQuestionByStepIndex = surveyAccountQuestionDao.findByIdAndSurveyAccountIdAndStepIndex(model.getQuestionId(), model.getSurveyAccountId(), model.getStepIndex());

        if (surveyAccountQuestionByStepIndex == null && model.getSurveyAccountId() == null) {
            /*
             * Trường hợp lưu câu trả lời mới
             * */
            /*
             * Lưu surveyAccount mới
             * */
            SurveyAccount surveyAccount = new SurveyAccount();
            surveyAccount.setSurvey(surveyDb);
            if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
                Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
                surveyAccount.setAccount(account);
            }
            String visitorUuid = CookieUtil.getCookieVisitorUuid(request);
            if (visitorUuid != null) {
                surveyAccount.setVisitorUuid(visitorUuid);
            }
            surveyAccount.setActionStatus(SurveyAccountConstant.ACTION_STATUS_SURVEY_IN_PROGRESS);
            surveyAccount.setActionDate(Instant.now());
            SurveyAccount surveyAccountSaved = surveyAccountDao.save(surveyAccount);

            doSurveyResponse.setSurveyAccount(surveyAccountSaved);

            saveAccountQuestionAndAnswer(surveyAccountSaved, surveyDb, surveyQuestionDb, model);

            return doSurveyResponse;
        }

        /*
         * Trường hợp update câu trả lời ở câu hỏi cũ
         *
         * Xóa toàn bộ câu trả lời ở các câu hỏi sau câu hỏi cũ này bằng cách xóa các question có surveyId, surveyAccountId và stepIndex >= step question cũ
         * */
        List<SurveyAccountQuestion> surveyAccountQuestionListForDelete = surveyAccountQuestionDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), model.getSurveyAccountId()),
                builder.greaterThanOrEqualTo(root.get(SurveyAccountQuestion._stepIndex), model.getStepIndex())
        ));

        for (SurveyAccountQuestion surveyAccountQuestion : surveyAccountQuestionListForDelete) {
            List<SurveyAccountAnswer> surveyAccountAnswerListForDelete = surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), surveyAccountQuestion.getId())
            ));
            surveyAccountAnswerDao.deleteAll(surveyAccountAnswerListForDelete);
        }

        surveyAccountQuestionDao.deleteAll(surveyAccountQuestionListForDelete);

        SurveyAccount surveyAccountDb = surveyAccountDao.findById(model.getSurveyAccountId()).orElse(null);
        if (surveyAccountDb == null) {
            throw new MyException("action.invalid");
        }

        doSurveyResponse.setSurveyAccount(surveyAccountDb);

        saveAccountQuestionAndAnswer(surveyAccountDb, surveyDb, surveyQuestionDb, model);

        return doSurveyResponse;
    }

    @Override
    public DoSurveyResponse getPrevQuestion(HttpServletRequest request, SubmitSurveyAnswerRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getQuestionId() == null) {
            throw new MyException("action.invalid");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion surveyQuestionDb = surveyQuestionDao.findById(model.getQuestionId()).orElse(null);
        if (surveyQuestionDb == null) {
            throw new MyException("action.invalid");
        }
        if (surveyQuestionDb.getIsFirst().equals(Boolean.TRUE)) {
            throw new MyException("action.invalid");
        }

        Integer surveyQuestionPrevId = getQuestionPrevId(model.getSurveyAccountId(), model.getQuestionId(), model.getStepIndex());
        if (surveyQuestionPrevId == null) {
            throw new MyException("action.invalid");
        }

        DoSurveyResponse doSurveyResponse = new DoSurveyResponse();
        doSurveyResponse.setSurvey(surveyDb);

        if (model.getSurveyAccountId() != null) {
            SurveyAccount surveyAccountDb = surveyAccountDao.findById(model.getSurveyAccountId()).orElse(null);
            if (surveyAccountDb == null) {
                throw new MyException("action.invalid");
            }
            doSurveyResponse.setSurveyAccount(surveyAccountDb);
        }

        /*
         * Tìm đến câu hỏi trước qua step của câu trả lời
         * */
        SurveyQuestion prevQuestion = surveyQuestionDao.findById(surveyQuestionPrevId).orElse(null);
        if (prevQuestion == null) {
            throw new MyException("action.invalid");
        }
        DoSurveyQuestionResponse doSurveyQuestionResponse = new DoSurveyQuestionResponse();
        doSurveyQuestionResponse.setId(prevQuestion.getId());
        doSurveyQuestionResponse.setSurveyId(prevQuestion.getSurvey().getId());
        doSurveyQuestionResponse.setName(prevQuestion.getName());
        doSurveyQuestionResponse.setDescription(prevQuestion.getDescription());
        doSurveyQuestionResponse.setImageThumbnail(prevQuestion.getImageThumbnail());
        doSurveyQuestionResponse.setImageFeature(prevQuestion.getImageFeature());
        doSurveyQuestionResponse.setAnswerDisplayType(prevQuestion.getAnswerDisplayType());
        doSurveyQuestionResponse.setAnswerSelectType(prevQuestion.getAnswerSelectType());
        doSurveyQuestionResponse.setIsAnswerRequired(prevQuestion.getIsAnswerRequired());
        doSurveyQuestionResponse.setIsFirst(prevQuestion.getIsFirst());

        List<SurveyAnswer> surveyAnswerPrevQuestionList = surveyAnswerDao.findAllBySurveyQuestionIdOrderByPriorityAsc(prevQuestion.getId());
        doSurveyQuestionResponse.setSurveyAnswerList(surveyAnswerPrevQuestionList);

        List<SurveyAccountAnswerV2Response> surveyAccountAnswerList = getSurveyAccountAnswerPrevStep(model.getSurveyAccountId(), prevQuestion.getId());
        doSurveyQuestionResponse.setSurveyAccountAnswerList(surveyAccountAnswerList);

        doSurveyResponse.setDoSurveyQuestionResponse(doSurveyQuestionResponse);

        doSurveyResponse.setSurvey(surveyDb);
        doSurveyResponse.setQuestionStep(model.getStepIndex());
        doSurveyResponse.setDone(false);

        return doSurveyResponse;
    }

    /*
     * Các câu hỏi không require câu trả lời, thường tất cả các đáp án sẽ được cấu hình đến chung 1 câu hỏi tiếp theo, client sẽ gửi all id answer lên để kiểm tra
     *
     * Các câu hỏi chọn nhiều thì tất cả các đáp án được cấu hình đến 1 câu hỏi duy nhất
     * */
    @Override
    public DoSurveyResponse getNextQuestion(HttpServletRequest request, SubmitSurveyAnswerRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getQuestionId() == null) {
            throw new MyException("action.invalid");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion surveyQuestionDb = surveyQuestionDao.findById(model.getQuestionId()).orElse(null);
        if (surveyQuestionDb == null) {
            throw new MyException("action.invalid");
        }
        validateQuestionRequireAnswer(surveyQuestionDb, model);

        boolean isNextToQuestionButNotAnswer = validateIsNextToQuestionButNotAnswer(model.getQuestionId(), model.getAnswerIdList());

        DoSurveyResponse doSurveyResponse = new DoSurveyResponse();
        doSurveyResponse.setSurvey(surveyDb);

        if (surveyQuestionDb.getIsAnswerRequired().equals(Boolean.TRUE)) {
            String visitorUuid = CookieUtil.getCookieVisitorUuid(request);
            if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
                Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
                for (Integer surveyAnswerId : model.getAnswerIdList()) {
                    SurveyAccountAnswer surveyAccountAnswer = surveyAccountAnswerDao.findOne((root, query, builder) -> builder.and(
                            builder.equal(root.get(SurveyAccountAnswer._surveyAnswer).get(SurveyAnswer._id), surveyAnswerId),
                            builder.equal(root.get(SurveyAccountAnswer._account).get(Account._id), account.getId())
                    )).orElse(null);
                    if (surveyAccountAnswer == null) {
                        throw new MyException("survey.account.submit.required");
                    }
                }
            } else {
                if (visitorUuid != null) {
                    SurveyAccount surveyAccountByVisitorUuid = surveyAccountDao.findBySurveyIdAndVisitorUuidAndAccountIsNull(model.getSurveyId(), visitorUuid);
                    for (Integer surveyAnswerId : model.getAnswerIdList()) {
                        SurveyAccountQuestion surveyAccountQuestion = surveyAccountQuestionDao.findBySurveyAccountIdAndSurveyQuestionId(surveyAccountByVisitorUuid.getId(), model.getQuestionId());
                        SurveyAccountAnswer surveyAccountAnswer = surveyAccountAnswerDao.findOne((root, query, builder) -> builder.and(
                                builder.equal(root.get(SurveyAccountAnswer._surveyAnswer).get(SurveyAnswer._id), surveyAnswerId),
                                builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(surveyAccountQuestion._id), surveyAccountQuestion.getId())
                        )).orElse(null);
                        if (surveyAccountAnswer == null) {
                            throw new MyException("survey.account.submit.required");
                        }
                    }
                }
            }
        }

        Integer surveyQuestionNextId = getQuestionNextId(model.getQuestionId(), model.getAnswerIdList());
        if (surveyQuestionNextId == null) {
            doSurveyResponse.setDone(true);
            return doSurveyResponse;
        }

        if (model.getSurveyAccountId() != null) {
            SurveyAccount surveyAccountDb = surveyAccountDao.findById(model.getSurveyAccountId()).orElse(null);
            if (surveyAccountDb == null) {
                throw new MyException("action.invalid");
            }
            doSurveyResponse.setSurveyAccount(surveyAccountDb);
        }

        if (isNextToQuestionButNotAnswer) {
            /*
            * Tìm đến câu hỏi tiếp theo qua step chung
            * */

            /*
            * Trường hợp ở câu hỏi đầu tiên surveyAccount sẽ là null => lấy câu hỏi thứ 2 rồi lưu surveyAccount
            * */
            SurveyQuestion nextQuestion = surveyQuestionDao.findById(surveyQuestionNextId).orElse(null);
            if (nextQuestion == null) {
                throw new MyException("action.invalid");
            }
            DoSurveyQuestionResponse doSurveyQuestionResponse = new DoSurveyQuestionResponse();
            doSurveyQuestionResponse.setId(nextQuestion.getId());
            doSurveyQuestionResponse.setSurveyId(nextQuestion.getSurvey().getId());
            doSurveyQuestionResponse.setName(nextQuestion.getName());
            doSurveyQuestionResponse.setDescription(nextQuestion.getDescription());
            doSurveyQuestionResponse.setImageThumbnail(nextQuestion.getImageThumbnail());
            doSurveyQuestionResponse.setImageFeature(nextQuestion.getImageFeature());
            doSurveyQuestionResponse.setAnswerDisplayType(nextQuestion.getAnswerDisplayType());
            doSurveyQuestionResponse.setAnswerSelectType(nextQuestion.getAnswerSelectType());
            doSurveyQuestionResponse.setIsAnswerRequired(nextQuestion.getIsAnswerRequired());
            doSurveyQuestionResponse.setIsFirst(nextQuestion.getIsFirst());

            List<SurveyAnswer> surveyAnswerNextQuestionList = surveyAnswerDao.findAllBySurveyQuestionIdOrderByPriorityAsc(nextQuestion.getId());
            doSurveyQuestionResponse.setSurveyAnswerList(surveyAnswerNextQuestionList);

            List<SurveyAccountAnswerV2Response> surveyAccountAnswerList = getSurveyAccountAnswerNextStep(model.getSurveyAccountId(), nextQuestion.getId());
            doSurveyQuestionResponse.setSurveyAccountAnswerList(surveyAccountAnswerList);

            doSurveyResponse.setDoSurveyQuestionResponse(doSurveyQuestionResponse);

            doSurveyResponse.setSurvey(surveyDb);
            doSurveyResponse.setQuestionStep(model.getStepIndex());
            doSurveyResponse.setDone(false);

            return doSurveyResponse;
        }

        /*
        * Tìm đến câu hỏi tiếp theo qua step của câu trả lời
        * */
        SurveyQuestion nextQuestion = surveyQuestionDao.findById(surveyQuestionNextId).orElse(null);
        if (nextQuestion == null) {
            throw new MyException("action.invalid");
        }
        DoSurveyQuestionResponse doSurveyQuestionResponse = new DoSurveyQuestionResponse();
        doSurveyQuestionResponse.setId(nextQuestion.getId());
        doSurveyQuestionResponse.setSurveyId(nextQuestion.getSurvey().getId());
        doSurveyQuestionResponse.setName(nextQuestion.getName());
        doSurveyQuestionResponse.setDescription(nextQuestion.getDescription());
        doSurveyQuestionResponse.setImageThumbnail(nextQuestion.getImageThumbnail());
        doSurveyQuestionResponse.setImageFeature(nextQuestion.getImageFeature());
        doSurveyQuestionResponse.setAnswerDisplayType(nextQuestion.getAnswerDisplayType());
        doSurveyQuestionResponse.setAnswerSelectType(nextQuestion.getAnswerSelectType());
        doSurveyQuestionResponse.setIsAnswerRequired(nextQuestion.getIsAnswerRequired());
        doSurveyQuestionResponse.setIsFirst(nextQuestion.getIsFirst());

        List<SurveyAnswer> surveyAnswerNextQuestionList = surveyAnswerDao.findAllBySurveyQuestionIdOrderByPriorityAsc(nextQuestion.getId());
        doSurveyQuestionResponse.setSurveyAnswerList(surveyAnswerNextQuestionList);

        List<SurveyAccountAnswerV2Response> surveyAccountAnswerList = getSurveyAccountAnswerNextStep(model.getSurveyAccountId(), nextQuestion.getId());
        doSurveyQuestionResponse.setSurveyAccountAnswerList(surveyAccountAnswerList);

        doSurveyResponse.setDoSurveyQuestionResponse(doSurveyQuestionResponse);

        doSurveyResponse.setSurvey(surveyDb);
        doSurveyResponse.setQuestionStep(model.getStepIndex());
        doSurveyResponse.setDone(false);

        return doSurveyResponse;
    }

    private DoSurveyResponse getDoSurveyResponse(DoSurveyRequest model, Survey surveyDb, DoSurveyResponse doSurveyResponse, SurveyAccount surveyAccount) {
        if (surveyAccount == null) {
            /*
             * User has never taken this survey before
             * */
            SurveyQuestion firstQuestion = surveyQuestionDao.findBySurveyIdAndIsFirst(model.getSurveyId(), Boolean.TRUE);
            DoSurveyQuestionResponse doSurveyQuestionResponse = new DoSurveyQuestionResponse();
            doSurveyQuestionResponse.setId(firstQuestion.getId());
            doSurveyQuestionResponse.setSurveyId(firstQuestion.getSurvey().getId());
            doSurveyQuestionResponse.setName(firstQuestion.getName());
            doSurveyQuestionResponse.setDescription(firstQuestion.getDescription());
            doSurveyQuestionResponse.setImageThumbnail(firstQuestion.getImageThumbnail());
            doSurveyQuestionResponse.setImageFeature(firstQuestion.getImageFeature());
            doSurveyQuestionResponse.setAnswerDisplayType(firstQuestion.getAnswerDisplayType());
            doSurveyQuestionResponse.setAnswerSelectType(firstQuestion.getAnswerSelectType());
            doSurveyQuestionResponse.setIsAnswerRequired(firstQuestion.getIsAnswerRequired());
            doSurveyQuestionResponse.setIsFirst(firstQuestion.getIsFirst());

            List<SurveyAnswer> surveyAnswerFirstQuestionList = surveyAnswerDao.findAllBySurveyQuestionIdOrderByPriorityAsc(firstQuestion.getId());
            doSurveyQuestionResponse.setSurveyAnswerList(surveyAnswerFirstQuestionList);

            doSurveyResponse.setDoSurveyQuestionResponse(doSurveyQuestionResponse);

            doSurveyResponse.setSurvey(surveyDb);
            doSurveyResponse.setQuestionStep(1);
            doSurveyResponse.setDone(false);
        } else {
            /*
             * User has previously taken this survey
             * */
            doSurveyResponse.setSurveyAccount(surveyAccount);
            doSurveyResponse.setSurvey(surveyDb);

            /*
             * Get the next question that is closest to the question that has already been answered
             * */
            SurveyAccountQuestion accountQuestion = surveyAccountQuestionDao.findBySurveyAccountIdAndStepIndexMax(surveyAccount.getId());
            SurveyAccountAnswer accountAnswer = surveyAccountAnswerDao.findBySurveyAccountQuestion(accountQuestion);
            SurveyQuestionStep questionStep = surveyQuestionStepDao.findBySurveyQuestionAndSurveyAnswer(accountQuestion.getSurveyQuestion(), accountAnswer.getSurveyAnswer());
            if (questionStep == null) {
                doSurveyResponse.setDone(true);
                return doSurveyResponse;
            }
            SurveyQuestion nextQuestion = surveyQuestionDao.findById(questionStep.getSurveyQuestionNext().getId()).orElse(null);

            if (nextQuestion != null) {
                DoSurveyQuestionResponse doSurveyQuestionResponse = new DoSurveyQuestionResponse();
                doSurveyQuestionResponse.setId(nextQuestion.getId());
                doSurveyQuestionResponse.setSurveyId(nextQuestion.getSurvey().getId());
                doSurveyQuestionResponse.setName(nextQuestion.getName());
                doSurveyQuestionResponse.setDescription(nextQuestion.getDescription());
                doSurveyQuestionResponse.setImageThumbnail(nextQuestion.getImageThumbnail());
                doSurveyQuestionResponse.setImageFeature(nextQuestion.getImageFeature());
                doSurveyQuestionResponse.setAnswerDisplayType(nextQuestion.getAnswerDisplayType());
                doSurveyQuestionResponse.setAnswerSelectType(nextQuestion.getAnswerSelectType());
                doSurveyQuestionResponse.setIsAnswerRequired(nextQuestion.getIsAnswerRequired());
                doSurveyQuestionResponse.setIsFirst(nextQuestion.getIsFirst());

                List<SurveyAnswer> surveyAnswerFirstQuestionList = surveyAnswerDao.findAllBySurveyQuestionIdOrderByPriorityAsc(nextQuestion.getId());
                doSurveyQuestionResponse.setSurveyAnswerList(surveyAnswerFirstQuestionList);

                doSurveyResponse.setDoSurveyQuestionResponse(doSurveyQuestionResponse);
                doSurveyResponse.setQuestionStep(accountQuestion.getStepIndex() + 1);
                doSurveyResponse.setDone(false);
            } else {
                doSurveyResponse.setDone(true);
            }
        }
        return doSurveyResponse;
    }

    private void validateQuestionRequireAnswer(SurveyQuestion surveyQuestion, SubmitSurveyAnswerRequest model) {
        if (surveyQuestion.getIsAnswerRequired().equals(Boolean.TRUE)) {
            if (model.getAnswerIdList().isEmpty()) {
                throw new MyException("survey.account.submit.required");
            }
            List<SurveyAnswer> surveyAnswerList = surveyAnswerDao.findAllById(model.getAnswerIdList());
            for (SurveyAnswer surveyAnswer : surveyAnswerList) {
                if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_INPUT) && model.getAnswerText() == null) {
                    throw new MyException("survey.account.submit.required");
                }
                if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_PROGRESS_BAR) && model.getAnswerDemandScore() == null) {
                    throw new MyException("survey.account.submit.required");
                }
                if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_TEXTAREA) && model.getAnswerImageUpload1() == null) {
                    throw new MyException("survey.account.submit.required");
                }
            }
        }
    }

    private void saveAccountQuestionAndAnswer(SurveyAccount surveyAccount, Survey survey, SurveyQuestion surveyQuestion, SubmitSurveyAnswerRequest model) {
        /*
         * Lưu surveyAccountQuestion
         * */
        SurveyAccountQuestion surveyAccountQuestion = new SurveyAccountQuestion();
        if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
            Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
            surveyAccountQuestion.setAccount(account);
        }
        surveyAccountQuestion.setSurveyAccount(surveyAccount);
        surveyAccountQuestion.setSurveyQuestion(surveyQuestion);
        surveyAccountQuestion.setSurvey(survey);
        surveyAccountQuestion.setStepIndex(model.getStepIndex());
        surveyAccountQuestion.setQuestionName(surveyQuestion.getName());
        surveyAccountQuestion.setQuestionDescription(surveyQuestion.getDescription());
        surveyAccountQuestion.setQuestionImageThumbnail(surveyQuestion.getImageThumbnail());
        surveyAccountQuestion.setQuestionImageFeature(surveyQuestion.getImageFeature());
        surveyAccountQuestion.setQuestionAnswerDisplayType(surveyQuestion.getAnswerDisplayType());
        surveyAccountQuestion.setQuestionAnswerSelectType(surveyQuestion.getAnswerSelectType());
        surveyAccountQuestion.setQuestionIsAnswerRequired(surveyQuestion.getIsAnswerRequired());
        surveyAccountQuestion.setQuestionIsFirst(surveyQuestion.getIsFirst());

        SurveyAccountQuestion surveyAccountQuestionSaved = surveyAccountQuestionDao.save(surveyAccountQuestion);

        /*
         * Lưu surveyAccountAnswer
         * */
        List<SurveyAnswer> surveyAnswerList = surveyAnswerDao.findAllByIdIn(model.getAnswerIdList());
        for (SurveyAnswer surveyAnswer : surveyAnswerList) {
            SurveyAccountAnswer surveyAccountAnswer = new SurveyAccountAnswer();
            if (!(authenticationFacade.getAuthentication() instanceof AnonymousAuthenticationToken)) {
                Account account = (Account) authenticationFacade.getAuthentication().getPrincipal();
                surveyAccountAnswer.setAccount(account);
            }
            surveyAccountAnswer.setSurveyAccountQuestion(surveyAccountQuestionSaved);
            surveyAccountAnswer.setSurveyAnswer(surveyAnswer);
            surveyAccountAnswer.setSurvey(survey);
            surveyAccountAnswer.setAnswerName(surveyAnswer.getName());
            surveyAccountAnswer.setAnswerDescription(surveyAnswer.getDescription());
            surveyAccountAnswer.setAnswerImageThumbnail(surveyAnswer.getImageThumbnail());
            surveyAccountAnswer.setAnswerImageFeature(surveyAnswer.getImageFeature());
            surveyAccountAnswer.setAnswerDisplayType(surveyAnswer.getDisplayType());
            surveyAccountAnswer.setAnswerPriority(surveyAnswer.getPriority());

            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_SELECT)) {
                surveyAccountAnswer.setAnswerDemandScore(surveyAnswer.getDemandScore());
            }
            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_INPUT)) {
                surveyAccountAnswer.setAnswerText(model.getAnswerText());
                surveyAccountAnswer.setAnswerDemandScore(surveyAnswer.getDemandScore());
            }
            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_PROGRESS_BAR)) {
                surveyAccountAnswer.setAnswerDemandScore(model.getAnswerDemandScore());
            }
            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_TEXTAREA)) {
                surveyAccountAnswer.setAnswerImageUpload1(model.getAnswerImageUpload1());
                surveyAccountAnswer.setAnswerImageUpload1(model.getAnswerImageUpload2());
                surveyAccountAnswer.setAnswerImageUpload1(model.getAnswerImageUpload3());
                surveyAccountAnswer.setAnswerImageUpload1(model.getAnswerImageUpload4());
                surveyAccountAnswer.setAnswerImageUpload1(model.getAnswerImageUpload5());
                surveyAccountAnswer.setAnswerDemandScore(surveyAnswer.getDemandScore());
            }
            surveyAccountAnswerDao.save(surveyAccountAnswer);
        }
    }

    private boolean validateIsNextToQuestionButNotAnswer(Integer surveyQuestionId, List<Integer> surveyAnswerIdList) {
        List<Integer> surveyAnswerIdForCheck = new ArrayList<>();
        boolean isNextToQuestionButNotAnswer = false;

        for (Integer surveyAnswerId : surveyAnswerIdList) {
            SurveyQuestionStep surveyQuestionStep = surveyQuestionStepDao.findOne((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyQuestionStep._surveyQuestion).get(SurveyQuestion._id), surveyQuestionId),
                    builder.equal(root.get(SurveyQuestionStep._surveyAnswer).get(SurveyAnswer._id), surveyAnswerId)
            )).orElse(null);
            if (surveyQuestionStep == null) {
                return false;
            }
            surveyAnswerIdForCheck.add(surveyQuestionStep.getSurveyQuestionNext().getId());
        }

        for (int i = 0; i < surveyAnswerIdForCheck.size(); i++) {
            for (int j = i + 1; j < surveyAnswerIdForCheck.size(); j++) {
                isNextToQuestionButNotAnswer = Objects.equals(surveyAnswerIdForCheck.get(i), surveyAnswerIdForCheck.get(j));
            }
        }

        return isNextToQuestionButNotAnswer;
    }

    private Integer getQuestionNextId(Integer surveyQuestionId, List<Integer> surveyAnswerIdList) {
        List<Integer> surveyQuestionNextIdForCheck = new ArrayList<>();
        boolean isSameId = false;
        Integer surveyQuestionNextId = null;

        for (Integer surveyAnswerId : surveyAnswerIdList) {
            SurveyQuestionStep surveyQuestionStep = surveyQuestionStepDao.findOne((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyQuestionStep._surveyQuestion).get(SurveyQuestion._id), surveyQuestionId),
                    builder.equal(root.get(SurveyQuestionStep._surveyAnswer).get(SurveyAnswer._id), surveyAnswerId)
            )).orElse(null);
            if (surveyQuestionStep == null) {
                return null;
            }
            surveyQuestionNextIdForCheck.add(surveyQuestionStep.getSurveyQuestionNext().getId());
        }

        if (surveyQuestionNextIdForCheck.size() == 1) {
            return surveyQuestionNextIdForCheck.get(0);
        }

        for (int i = 0; i < surveyQuestionNextIdForCheck.size(); i++) {
            for (int j = i + 1; j < surveyQuestionNextIdForCheck.size(); j++) {
                isSameId = Objects.equals(surveyQuestionNextIdForCheck.get(i), surveyQuestionNextIdForCheck.get(j));
            }
        }

        if (isSameId) {
            surveyQuestionNextId = surveyQuestionNextIdForCheck.get(0);
        }

        return surveyQuestionNextId;
    }

    private List<SurveyAccountAnswerV2Response> getSurveyAccountAnswerNextStep(Integer surveyAccountId, Integer surveyQuestionId) {
        SurveyAccountQuestion accountQuestion = surveyAccountQuestionDao.findOne((root, query, builder) -> builder.and(
                builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountId),
                builder.equal(root.get(SurveyAccountQuestion._surveyQuestion).get(SurveyQuestion._id), surveyQuestionId)
        )).orElse(null);
        if (accountQuestion == null) {
            return null;
        }
        return surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), accountQuestion.getId())
        )).stream().map(surveyAccountAnswer -> {
            SurveyAccountAnswerV2Response surveyAccountAnswerV2Response = new SurveyAccountAnswerV2Response();
            surveyAccountAnswerV2Response.setSurveyAnswerId(surveyAccountAnswer.getSurveyAnswer().getId());
            surveyAccountAnswerV2Response.setAnswerDisplayType(surveyAccountAnswer.getAnswerDisplayType());
            surveyAccountAnswerV2Response.setAnswerDemandScore(surveyAccountAnswer.getAnswerDemandScore());
            surveyAccountAnswerV2Response.setAnswerText(surveyAccountAnswer.getAnswerText());
            surveyAccountAnswerV2Response.setAnswerImageUpload1(surveyAccountAnswer.getAnswerImageUpload1());
            surveyAccountAnswerV2Response.setAnswerImageUpload2(surveyAccountAnswer.getAnswerImageUpload2());
            surveyAccountAnswerV2Response.setAnswerImageUpload3(surveyAccountAnswer.getAnswerImageUpload3());
            surveyAccountAnswerV2Response.setAnswerImageUpload4(surveyAccountAnswer.getAnswerImageUpload4());
            surveyAccountAnswerV2Response.setAnswerImageUpload5(surveyAccountAnswer.getAnswerImageUpload5());

            return surveyAccountAnswerV2Response;
        }).collect(Collectors.toList());
    }

    private Integer getQuestionPrevId(Integer surveyAccountId, Integer surveyQuestionId, Integer stepIndex) {
        if (surveyAccountId == null) {
            SurveyQuestionStep surveyQuestionStep = surveyQuestionStepDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyQuestionStep._surveyQuestionNext).get(SurveyQuestion._id), surveyQuestionId)
            )).get(0);
            if (surveyQuestionStep == null) {
                return null;
            }
            return surveyQuestionStep.getSurveyQuestion().getId();
        } else {
            SurveyAccount surveyAccount = surveyAccountDao.findById(surveyAccountId).orElse(null);
            if (surveyAccount == null) {
                return null;
            }

            if (stepIndex != null) {
                SurveyAccountQuestion surveyAccountQuestion = surveyAccountQuestionDao.findOne((root, query, builder) -> builder.and(
                        builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountId),
                        builder.equal(root.get(SurveyAccountQuestion._stepIndex), stepIndex)
                )).orElse(null);
                if (surveyAccountQuestion != null) {
                    return surveyAccountQuestion.getSurveyQuestion().getId();
                }
            }

            SurveyAccountQuestion accountQuestionLatest = surveyAccountQuestionDao.findBySurveyAccountIdAndStepIndexMax(surveyAccount.getId());
            if (accountQuestionLatest == null) {
                return null;
            }

            List<Integer> surveyQuestionIds = surveyAccountQuestionDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountId)
            )).stream().map(surveyAccountQuestion -> surveyAccountQuestion.getSurveyQuestion().getId()).toList();
            if (surveyQuestionIds.isEmpty()) {
                return null;
            }

            if (surveyQuestionIds.contains(surveyQuestionId)) {
                if (surveyQuestionIds.indexOf(surveyQuestionId) == 0) {
                    return surveyQuestionIds.get(0);
                }
                return surveyQuestionIds.get(surveyQuestionIds.indexOf(surveyQuestionId) - 1);
            }

            return getPrevStep(new ArrayList<>(surveyQuestionIds), accountQuestionLatest.getSurveyQuestion().getId(), surveyQuestionId);
        }
    }

    private Integer getPrevStep(List<Integer> ids, Integer surveyQuestionId, Integer surveyQuestionIdFromClient) {
        SurveyQuestionStep step = surveyQuestionStepDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(SurveyQuestionStep._surveyQuestion).get(SurveyQuestion._id), surveyQuestionId)
        )).get(0);
        if (step.getSurveyQuestionNext().getId().equals(surveyQuestionIdFromClient)) {
            return ids.get(ids.size() - 1);
        }
        ids.add(step.getSurveyQuestionNext().getId());
        return getPrevStep(ids, step.getSurveyQuestionNext().getId(), surveyQuestionIdFromClient);
    }

    private List<SurveyAccountAnswerV2Response> getSurveyAccountAnswerPrevStep(Integer surveyAccountId, Integer surveyQuestionId) {
        SurveyAccountQuestion accountQuestion = surveyAccountQuestionDao.findOne((root, query, builder) -> builder.and(
                builder.equal(root.get(SurveyAccountQuestion._surveyAccount).get(SurveyAccount._id), surveyAccountId),
                builder.equal(root.get(SurveyAccountQuestion._surveyQuestion).get(SurveyQuestion._id), surveyQuestionId)
        )).orElse(null);
        if (accountQuestion == null) {
            return null;
        }
        return surveyAccountAnswerDao.findAll((root, query, builder) -> builder.and(
           builder.equal(root.get(SurveyAccountAnswer._surveyAccountQuestion).get(SurveyAccountQuestion._id), accountQuestion.getId())
        )).stream().map(surveyAccountAnswer -> {
            SurveyAccountAnswerV2Response surveyAccountAnswerV2Response = new SurveyAccountAnswerV2Response();
            surveyAccountAnswerV2Response.setSurveyAnswerId(surveyAccountAnswer.getSurveyAnswer().getId());
            surveyAccountAnswerV2Response.setAnswerDisplayType(surveyAccountAnswer.getAnswerDisplayType());
            surveyAccountAnswerV2Response.setAnswerDemandScore(surveyAccountAnswer.getAnswerDemandScore());
            surveyAccountAnswerV2Response.setAnswerText(surveyAccountAnswer.getAnswerText());
            surveyAccountAnswerV2Response.setAnswerImageUpload1(surveyAccountAnswer.getAnswerImageUpload1());
            surveyAccountAnswerV2Response.setAnswerImageUpload2(surveyAccountAnswer.getAnswerImageUpload2());
            surveyAccountAnswerV2Response.setAnswerImageUpload3(surveyAccountAnswer.getAnswerImageUpload3());
            surveyAccountAnswerV2Response.setAnswerImageUpload4(surveyAccountAnswer.getAnswerImageUpload4());
            surveyAccountAnswerV2Response.setAnswerImageUpload5(surveyAccountAnswer.getAnswerImageUpload5());

            return surveyAccountAnswerV2Response;
        }).collect(Collectors.toList());
    }
}

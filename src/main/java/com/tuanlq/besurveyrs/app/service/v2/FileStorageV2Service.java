package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.web.v2.file.ImageFileResponse;

public interface FileStorageV2Service {

    ImageFileResponse doViewImage(String fileName);

}

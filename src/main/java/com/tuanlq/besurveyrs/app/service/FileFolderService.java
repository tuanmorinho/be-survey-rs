package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.app.web.folder.FileFolderListResponse;
import com.tuanlq.besurveyrs.app.web.folder.FileFolderRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;

public interface FileFolderService extends Service<FileFolder, Integer> {

    FileFolderListResponse viewFolder(FileFolder bo);

    FileFolder doInsertFolder(FileFolderRequest model);

}

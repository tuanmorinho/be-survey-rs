package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestionStep;
import com.tuanlq.besurveyrs.app.dao.SurveyAnswerDao;
import com.tuanlq.besurveyrs.app.dao.SurveyQuestionDao;
import com.tuanlq.besurveyrs.app.dao.SurveyQuestionStepDao;
import com.tuanlq.besurveyrs.app.web.surveyquestionstep.StepMapping;
import com.tuanlq.besurveyrs.app.web.surveyquestionstep.SurveyQuestionStepRequest;
import com.tuanlq.besurveyrs.app.web.surveyquestionstep.SurveyQuestionStepResponse;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SurveyQuestionStepServiceImpl implements SurveyQuestionStepService {

    private final SurveyQuestionStepDao surveyQuestionStepDao;
    private final SurveyQuestionDao surveyQuestionDao;
    private final SurveyAnswerDao surveyAnswerDao;

    @Override
    public ResponseEntity<?> doMappingStep(List<SurveyQuestionStepRequest> model) {
        if (model.isEmpty()) {
            throw new MyException("action.invalid");
        }

        for (SurveyQuestionStepRequest stepMapping : model) {
            if (stepMapping.getSurveyQuestionId() != null) {

                SurveyQuestion surveyQuestionDb = surveyQuestionDao.findById(stepMapping.getSurveyQuestionId()).orElse(null);
                if (surveyQuestionDb == null) {
                    throw new MyException("survey.question.step.question.is.null", new Object[]{stepMapping.getSurveyQuestionId()});
                }

                List<SurveyQuestionStep> surveyQuestionStepList = surveyQuestionStepDao.findBySurveyQuestionId(surveyQuestionDb.getId());
                if (!surveyQuestionStepList.isEmpty()) {
                    surveyQuestionStepDao.deleteAll(surveyQuestionStepList);
                }

                for (StepMapping step : stepMapping.getStep()) {
                    if (step.getSurveyAnswerId() != null && step.getSurveyQuestionNextId() != null) {
                        SurveyQuestion surveyQuestionNextDb = surveyQuestionDao.findById(step.getSurveyQuestionNextId()).orElse(null);
                        if (surveyQuestionNextDb == null) {
                            throw new MyException("survey.question.step.question.is.null", new Object[]{step.getSurveyQuestionNextId()});
                        }

                        SurveyAnswer surveyAnswerDb = surveyAnswerDao.findById(step.getSurveyAnswerId()).orElse(null);
                        if (surveyAnswerDb == null) {
                            throw new MyException("survey.question.step.answer.is.null", new Object[]{step.getSurveyAnswerId()});
                        }

                        SurveyQuestionStep surveyQuestionStep = new SurveyQuestionStep();
                        surveyQuestionStep.setSurveyQuestion(surveyQuestionDb);
                        surveyQuestionStep.setSurveyAnswer(surveyAnswerDb);
                        surveyQuestionStep.setSurveyQuestionNext(surveyQuestionNextDb);

                        surveyQuestionStepDao.save(surveyQuestionStep);
                    }
                }
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public List<SurveyQuestionStepResponse> getMappingDetail(Integer surveyId) {
        if (surveyId == null) {
            throw new MyException("action.invalid");
        }

        List<SurveyQuestionStep> surveyQuestionStepList = surveyQuestionStepDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(SurveyQuestionStep._surveyQuestion).get(SurveyQuestion._survey).get(Survey._id), surveyId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });

        Map<SurveyQuestion, List<SurveyQuestionStep>> surveyQuestionStepMap = surveyQuestionStepList
                .stream()
                .collect(Collectors.groupingBy(SurveyQuestionStep::getSurveyQuestion, LinkedHashMap::new, Collectors.toList()));

        List<SurveyQuestionStepResponse> response = new ArrayList<>();

        for (SurveyQuestion questionKey : surveyQuestionStepMap.keySet()) {
            SurveyQuestionStepResponse surveyQuestionStepResponse = new SurveyQuestionStepResponse();
            if (questionKey != null && questionKey.getId() != null) {
                surveyQuestionStepResponse.setSurveyQuestionId(questionKey.getId());
                surveyQuestionStepResponse.setSurveyQuestion(questionKey);

                List<SurveyAnswer> surveyAnswerList = surveyAnswerDao.findAll((root, query, builder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(builder.equal(root.get(SurveyAnswer._surveyQuestion).get(SurveyQuestion._id), questionKey.getId()));
                    return builder.and(predicates.toArray(new Predicate[0]));
                });
                surveyQuestionStepResponse.setSurveyAnswer(surveyAnswerList);

                List<StepMapping> step = new ArrayList<>();
                for (SurveyQuestionStep surveyQuestionStep : surveyQuestionStepMap.get(questionKey)) {
                    StepMapping stepMapping = getStepMapping(surveyQuestionStep);
                    step.add(stepMapping);
                }
                surveyQuestionStepResponse.setStep(step);

                response.add(surveyQuestionStepResponse);
            }
        }

        return response;
    }

    private static StepMapping getStepMapping(SurveyQuestionStep surveyQuestionStep) {
        StepMapping stepMapping = new StepMapping();
        if (surveyQuestionStep.getSurveyAnswer() != null && surveyQuestionStep.getSurveyAnswer().getId() != null) {
            stepMapping.setSurveyAnswerId(surveyQuestionStep.getSurveyAnswer().getId());
            stepMapping.setSurveyAnswer(surveyQuestionStep.getSurveyAnswer());
        }
        if (surveyQuestionStep.getSurveyQuestionNext() != null && surveyQuestionStep.getSurveyQuestionNext().getId() != null) {
            stepMapping.setSurveyQuestionNextId(surveyQuestionStep.getSurveyQuestionNext().getId());
            stepMapping.setSurveyQuestionNext(surveyQuestionStep.getSurveyQuestionNext());
        }
        return stepMapping;
    }
}

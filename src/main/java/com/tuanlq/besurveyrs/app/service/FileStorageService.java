package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.FileStorage;
import com.tuanlq.besurveyrs.app.web.file.FileDownloadResponse;
import com.tuanlq.besurveyrs.app.web.file.FileResponse;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService extends Service<FileStorage, Integer> {

    FileResponse doUploadFile(MultipartFile file, Integer folderId);

    FileDownloadResponse doDownloadFile(String fileName);

}

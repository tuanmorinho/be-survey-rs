package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import com.tuanlq.besurveyrs.app.common.constant.SurveyAnswerConstant;
import com.tuanlq.besurveyrs.app.common.constant.SurveyConstant;
import com.tuanlq.besurveyrs.app.dao.SurveyAnswerDao;
import com.tuanlq.besurveyrs.app.dao.SurveyDao;
import com.tuanlq.besurveyrs.app.dao.SurveyQuestionDao;
import com.tuanlq.besurveyrs.app.web.surveyanswer.SurveyAnswerRequest;
import com.tuanlq.besurveyrs.app.web.surveyanswer.SurveyAnswerResponse;
import com.tuanlq.besurveyrs.app.web.surveyanswer.SurveyAnswerSearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractService;
import com.tuanlq.besurveyrs.infrastructure.core.utils.PageUtil;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SurveyAnswerServiceImpl extends AbstractService<SurveyAnswer, Integer> implements SurveyAnswerService {

    private final SurveyAnswerDao surveyAnswerDao;
    private final SurveyQuestionDao surveyQuestionDao;
    private final SurveyDao surveyDao;

    @Override
    public Page<SurveyAnswer> findBySearch(SurveyAnswerSearchRequest model) {
        if (model.getSurveyId() == null) {
            throw new MyException("survey.answer.surveyId.required");
        }
        if (model.getSurveyQuestionId() == null) {
            throw new MyException("survey.answer.surveyQuestionId.required");
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion surveyQuestionDb = surveyQuestionDao.findById(model.getSurveyQuestionId()).orElse(null);
        if (surveyQuestionDb == null) {
            throw new MyException("action.invalid");
        }

        Pageable pageable = PageUtil.getPageRequest(model);
        return surveyAnswerDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if (surveyQuestionDb.getId() != null) {
                predicates.add(builder.equal(root.get(SurveyAnswer._surveyQuestion).get(SurveyQuestion._id), surveyQuestionDb.getId()));
            }

            query.orderBy(builder.desc(root.get(SurveyAnswer._createdDate)), builder.desc(root.get(SurveyAnswer._id)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public List<SurveyAnswer> findAllSurveyAnswer(Integer surveyQuestionId) {
        if (surveyQuestionId == null) {
            throw new MyException("survey.answer.surveyQuestionId.required");
        }
        return surveyAnswerDao.findAllBySurveyQuestionIdSortByDirectionAsc(surveyQuestionId);
    }

    @Override
    public SurveyAnswerResponse getSurveyAnswerDetail(Integer id) {
        if (id == null) {
            throw new MyException("error.get");
        }

        SurveyAnswer dbBo = surveyAnswerDao.findById(id).orElse(null);
        if (dbBo == null) {
            throw new MyException("error.get");
        }

        SurveyAnswerResponse surveyAnswerResponse = new SurveyAnswerResponse();
        if (dbBo.getSurveyQuestion() != null) {
            surveyAnswerResponse.setSurveyQuestionId(dbBo.getSurveyQuestion().getId());
            surveyAnswerResponse.setSurveyId(dbBo.getSurveyQuestion().getSurvey().getId());
        }
        surveyAnswerResponse.setId(dbBo.getId());
        surveyAnswerResponse.setName(dbBo.getName());
        surveyAnswerResponse.setDescription(dbBo.getDescription());
        surveyAnswerResponse.setImageThumbnail(dbBo.getImageThumbnail());
        surveyAnswerResponse.setImageFeature(dbBo.getImageFeature());
        surveyAnswerResponse.setDisplayType(dbBo.getDisplayType());
        surveyAnswerResponse.setPriority(dbBo.getPriority());
        surveyAnswerResponse.setDemandScore(dbBo.getDemandScore());

        return surveyAnswerResponse;
    }

    @Override
    public SurveyAnswer doCreateSurveyAnswer(SurveyAnswerRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getName() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getSurveyQuestionId() == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion dbSurveyQuestionBo = surveyQuestionDao.findById(model.getSurveyQuestionId()).orElse(null);
        if (dbSurveyQuestionBo == null) {
            throw new MyException("action.invalid");
        }

        boolean isSurveyAnswerDisplayTypeNotSelect = validateCreateSurveyAnswerDisplayTypeNotSelect(dbSurveyQuestionBo.getId());
        if (!isSurveyAnswerDisplayTypeNotSelect) {
            throw new MyException("create.survey.answer.not.type.select");
        }

        SurveyAnswer surveyAnswer = new SurveyAnswer();
        surveyAnswer.setSurveyQuestion(dbSurveyQuestionBo);
        surveyAnswer.setName(model.getName());
        surveyAnswer.setDescription(model.getDescription());
        surveyAnswer.setImageFeature(model.getImageFeature());
        surveyAnswer.setImageThumbnail(model.getImageThumbnail());
        surveyAnswer.setDisplayType(model.getDisplayType());
        surveyAnswer.setDemandScore(model.getDemandScore());
        surveyAnswer.setPriority(0);
        Integer maxPriorityAnswer = surveyAnswerDao.getMaxPriorityAnswer(model.getSurveyQuestionId());
        if (maxPriorityAnswer != null) {
            surveyAnswer.setPriority(maxPriorityAnswer + 1);
        }

        surveyAnswerDao.save(surveyAnswer);

        return surveyAnswer;
    }

    @Override
    public SurveyAnswer doUpdateSurveyAnswer(SurveyAnswerRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getId() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getName() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getSurveyQuestionId() == null) {
            throw new MyException("action.invalid");
        }

        SurveyAnswer dbBo = surveyAnswerDao.findById(model.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }

        SurveyQuestion dbSurveyQuestionBo = surveyQuestionDao.findById(model.getSurveyQuestionId()).orElse(null);
        if (dbSurveyQuestionBo == null) {
            throw new MyException("action.invalid");
        }

        dbBo.setName(model.getName());
        dbBo.setDescription(model.getDescription());
        dbBo.setImageThumbnail(model.getImageThumbnail());
        dbBo.setImageFeature(model.getImageFeature());
        dbBo.setDisplayType(model.getDisplayType());
        dbBo.setDemandScore(model.getDemandScore());

        boolean isSurveyAnswerDisplayTypeNotSelect = validateCreateSurveyAnswerDisplayTypeNotSelect(dbSurveyQuestionBo.getId());
        if (!isSurveyAnswerDisplayTypeNotSelect) {
            throw new MyException("update.survey.answer.not.type.select");
        }

        surveyAnswerDao.save(dbBo);

        return dbBo;
    }

    @Override
    public boolean validateCreateSurveyAnswer(SurveyAnswerRequest model) {
        if (model == null) {
            return false;
        }

        if (model.getSurveyId() == null) {
            return false;
        }

        Survey surveyDb = surveyDao.findById(model.getSurveyId()).orElse(null);
        if (surveyDb == null) {
            return false;
        }
        if (surveyDb.getStatus().equals(SurveyConstant.STATUS_ACTIVE) || surveyDb.getIsTrash().equals(Boolean.TRUE)) {
            return false;
        }

        if (model.getSurveyQuestionId() == null) {
            return false;
        }

        SurveyQuestion dbSurveyQuestionBo = surveyQuestionDao.findById(model.getSurveyQuestionId()).orElse(null);
        if (dbSurveyQuestionBo == null) {
            return false;
        }

        return validateCreateSurveyAnswerDisplayTypeNotSelect(dbSurveyQuestionBo.getId());
    }

    private boolean validateCreateSurveyAnswerDisplayTypeNotSelect(Integer surveyQuestionId) {
        boolean isSurveyAnswerDisplayTypeSelect = true;
        List<SurveyAnswer> surveyAnswerDbList = surveyAnswerDao.findAllBySurveyQuestionId(surveyQuestionId);
        if (surveyAnswerDbList.isEmpty()) return true;
        for (SurveyAnswer surveyAnswer : surveyAnswerDbList) {
            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_INPUT)) {
                isSurveyAnswerDisplayTypeSelect = false;
                break;
            }
            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_PROGRESS_BAR)) {
                isSurveyAnswerDisplayTypeSelect = false;
                break;
            }
            if (surveyAnswer.getDisplayType().equals(SurveyAnswerConstant.DISPLAY_TYPE_TEXTAREA)) {
                isSurveyAnswerDisplayTypeSelect = false;
                break;
            }
        }

        return isSurveyAnswerDisplayTypeSelect;
    }
}

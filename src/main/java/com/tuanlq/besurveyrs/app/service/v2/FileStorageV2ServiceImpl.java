package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.FileStorage;
import com.tuanlq.besurveyrs.app.dao.FileStorageDao;
import com.tuanlq.besurveyrs.app.web.v2.file.ImageFileResponse;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FileStorageV2ServiceImpl implements FileStorageV2Service {

    private final FileStorageDao fileStorageDao;

    @Override
    public ImageFileResponse doViewImage(String fileName) {
        if (fileName == null || fileName.length() == 0) {
            throw new MyException("action.invalid");
        }

        FileStorage dbBo = fileStorageDao.findByName(fileName);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }
        if (!dbBo.getType().contains("image")) {
            throw new MyException("action.invalid");
        }

        ImageFileResponse imageFileResponse = new ImageFileResponse();
        imageFileResponse.setType(dbBo.getType());
        imageFileResponse.setData(FileUtil.decompressFile(dbBo.getData()));

        return imageFileResponse;
    }
}

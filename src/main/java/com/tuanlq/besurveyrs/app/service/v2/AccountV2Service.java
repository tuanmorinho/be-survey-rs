package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.web.v2.account.AccountV2Request;
import com.tuanlq.besurveyrs.app.web.v2.account.AccountV2Response;

public interface AccountV2Service {

    AccountV2Response register(AccountV2Request model);

    AccountV2Response login(AccountV2Request model);
}

package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.infrastructure.core.service.Service;

public interface AccountService extends Service<Account, Integer> {
}

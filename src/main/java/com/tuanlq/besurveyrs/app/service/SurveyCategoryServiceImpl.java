package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.dao.SurveyCategoryDao;
import com.tuanlq.besurveyrs.app.dao.SurveyDao;
import com.tuanlq.besurveyrs.app.web.surveycategory.SurveyCategorySearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractService;
import com.tuanlq.besurveyrs.infrastructure.core.utils.PageUtil;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SurveyCategoryServiceImpl extends AbstractService<SurveyCategory, Integer> implements SurveyCategoryService {

    private final SurveyCategoryDao surveyCategoryDao;
    private final SurveyDao surveyDao;

    @Override
    public Page<SurveyCategory> findBySearch(SurveyCategorySearchRequest model) {
        Pageable pageable = PageUtil.getPageRequest(model);
        return surveyCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if (model.getKeyword() != null) {
                predicates.add(builder.like(root.get(SurveyCategory._name), "%" + model.getKeyword() + "%"));
            }

            query.orderBy(builder.asc(root.get(SurveyCategory._priority)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public SurveyCategory get(Integer id) {
        if (id == null) {
            throw new MyException("action.invalid");
        }

        SurveyCategory dbBo = surveyCategoryDao.findById(id).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }

        return dbBo;
    }

    @Override
    public SurveyCategory doInsert(SurveyCategory bo) {
        if (bo == null) {
            throw new MyException("action.invalid");
        }

        bo.setPriority(0);
        Integer priorityMax = surveyCategoryDao.getPriorityMax();
        if (priorityMax != null) {
            bo.setPriority(priorityMax + 1);
        }
        super.doInsert(bo);

        return bo;
    }

    @Override
    public SurveyCategory doDelete(SurveyCategory bo) {
        if (bo == null) {
            throw new MyException("action.invalid");
        }

        if (bo.getId() == null) {
            throw new MyException("action.invalid");
        }

        List<Survey> surveyListByCategoryId = surveyDao.findAllBySurveyCategoryId(bo.getId());
        for (Survey survey : surveyListByCategoryId) {
            survey.setSurveyCategory(null);
        }

        surveyCategoryDao.delete(bo);

        return bo;
    }
}

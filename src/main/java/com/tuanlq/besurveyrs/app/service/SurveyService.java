package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.web.survey.SurveyRequest;
import com.tuanlq.besurveyrs.app.web.survey.SurveyResponse;
import com.tuanlq.besurveyrs.app.web.survey.SurveySearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.service.StatusService;
import org.springframework.data.domain.Page;

public interface SurveyService extends StatusService<Survey> {

    Page<Survey> findBySearch(SurveySearchRequest model);

    SurveyResponse getSurveyDetail(Integer id);

    Survey doCreateSurvey(SurveyRequest model);

    Survey doUpdateSurvey(SurveyRequest model);
}

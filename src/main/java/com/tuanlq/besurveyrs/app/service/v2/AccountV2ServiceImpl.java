package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.app.bo.Role;
import com.tuanlq.besurveyrs.app.common.constant.AccountConstant;
import com.tuanlq.besurveyrs.app.common.enumerate.RoleEnum;
import com.tuanlq.besurveyrs.app.dao.AccountDao;
import com.tuanlq.besurveyrs.app.dao.RoleDao;
import com.tuanlq.besurveyrs.app.web.authentication.AuthenticationResponse;
import com.tuanlq.besurveyrs.app.web.v2.account.AccountV2Request;
import com.tuanlq.besurveyrs.app.web.v2.account.AccountV2Response;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.utils.JwtUtil;
import com.tuanlq.besurveyrs.infrastructure.core.utils.StringUtils;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class AccountV2ServiceImpl implements AccountV2Service {

    private final AccountDao accountDao;
    private final RoleDao roleDao;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtils;
    private final AuthenticationManager authenticationManager;

    @Override
    public AccountV2Response register(AccountV2Request model) {
        if (!model.getRegisterType().equals(AccountConstant.REGISTER_TYPE_USER)) {
            throw new MyException("action.invalid");
        }

        if (model.getFullName() == null) {
            throw new MyException("account.fullName.invalid");
        }
        if (model.getMobile() == null && model.getEmail() == null) {
            throw new MyException("account.identify.invalid");
        }

        Account dbBo = findAccountByMobileOrEmail(model);

        if (dbBo != null) {
            throw new MyException("error.register.duplicate_username");
        }

        Role dbRoleBo = roleDao.getByName(RoleEnum.USER.name());
        if (dbRoleBo == null) {
            throw new MyException("action.invalid");
        }

        Account account = new Account();
        account.setFullName(model.getFullName());
        account.setAvatarUrl(model.getAvatarUrl());
        account.setMobile(model.getMobile());
        account.setEmail(model.getEmail());
        if (model.getMobile() != null) {
            account.setUsername(model.getMobile() + "_" + StringUtils.getRandomCode(8));
        }
        if (model.getEmail() != null) {
            account.setUsername(model.getEmail() + "_" + StringUtils.getRandomCode(8));
        }
        account.setPassword(passwordEncoder.encode(model.getPassword()));
        account.setRegisterType(AccountConstant.REGISTER_TYPE_USER);
        account.setRoles(Collections.singleton(dbRoleBo));
        accountDao.save(account);

        String token = jwtUtils.generateToken(generateClaims(account), account);
        AccountV2Response accountV2Response = new AccountV2Response();
        accountV2Response.setToken(token);

        return accountV2Response;
    }

    @Override
    public AccountV2Response login(AccountV2Request model) {
        if (!model.getRegisterType().equals(AccountConstant.REGISTER_TYPE_USER)) {
            throw new MyException("action.invalid");
        }

        if ((model.getMobile() == null && model.getEmail() == null) || model.getPassword() == null) {
            throw new MyException("account.identify.invalid");
        }

        Account dbBo = findAccountByMobileOrEmail(model);

        if (dbBo == null) {
            throw new MyException("error.login.user_not_found");
        }

        if (!passwordEncoder.matches(model.getPassword(), dbBo.getPassword())) {
            throw new MyException("error.login.user_not_found");
        }

        model.setUsername(dbBo.getUsername());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        model.getUsername(),
                        model.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = jwtUtils.generateToken(generateClaims(dbBo), (UserDetails) authentication.getPrincipal());
        AccountV2Response accountV2Response = new AccountV2Response();
        accountV2Response.setToken(token);

        return accountV2Response;
    }

    private Account findAccountByMobileOrEmail(AccountV2Request model) {
        return accountDao.findOne((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (model.getMobile() != null) {
                predicates.add(builder.equal(root.get(Account._mobile), model.getMobile()));
            }
            if (model.getEmail() != null) {
                predicates.add(builder.equal(root.get(Account._email), model.getEmail()));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        }).orElse(null);
    }

    private Map<String, Map<String, String>> generateClaims(Account bo) {
        Map<String, String> accountInfoClaimMap = new HashMap<>();
        accountInfoClaimMap.put("fullName", bo.getFullName());
        accountInfoClaimMap.put("avatarUrl", bo.getAvatarUrl());
        accountInfoClaimMap.put("mobile", bo.getMobile());

        Map<String, Map<String, String>> extraClaims = new HashMap<>();
        extraClaims.put("account", accountInfoClaimMap);

        return extraClaims;
    }
}

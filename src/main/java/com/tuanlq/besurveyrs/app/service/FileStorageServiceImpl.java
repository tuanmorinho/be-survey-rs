package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.app.bo.FileStorage;
import com.tuanlq.besurveyrs.app.dao.FileFolderDao;
import com.tuanlq.besurveyrs.app.dao.FileStorageDao;
import com.tuanlq.besurveyrs.app.web.file.FileDownloadResponse;
import com.tuanlq.besurveyrs.app.web.file.FileResponse;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractService;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class FileStorageServiceImpl extends AbstractService<FileStorage, Integer> implements FileStorageService {

    private final FileStorageDao fileStorageDao;
    private final FileFolderDao fileFolderDao;

    @Override
    public FileResponse doUploadFile(MultipartFile file, Integer folderId) {
        if (file == null) {
            throw new MyException("action.invalid");
        }

        try {
            FileStorage fileStorage = new FileStorage();

            if (folderId == null) {
                FileStorage fileSameName = fileStorageDao.findByName(file.getOriginalFilename());
                if (fileSameName == null) {
                    fileStorage.setName(file.getOriginalFilename());
                } else {
                    int no = fileSameName.getNo() + 1;
                    fileStorage.setName(file.getOriginalFilename() + "(" + no + ")");
                    fileSameName.setNo(fileSameName.getNo() + 1);
                    fileStorageDao.save(fileSameName);
                }
            } else {
                FileFolder folderOfFileUpload = fileFolderDao.findById(folderId).orElse(null);
                if (folderOfFileUpload == null) {
                    throw new MyException("action.invalid");
                }
                fileStorage.setFileFolder(folderOfFileUpload);

                FileStorage fileSameNameAndFolder = fileStorageDao.findByNameAndFileFolder(folderOfFileUpload.getName() + "_" + file.getOriginalFilename(), folderOfFileUpload);
                if (fileSameNameAndFolder == null) {
                    fileStorage.setName(folderOfFileUpload.getName() + "_" + file.getOriginalFilename());
                } else {
                    int no = fileSameNameAndFolder.getNo() + 1;
                    fileStorage.setName(folderOfFileUpload.getName() + "_" + file.getOriginalFilename() + "(" + no + ")");
                    fileSameNameAndFolder.setNo(fileSameNameAndFolder.getNo() + 1);
                    fileStorageDao.save(fileSameNameAndFolder);
                }
            }
            fileStorage.setNo(0);
            fileStorage.setType(file.getContentType());
            fileStorage.setData(FileUtil.compressFile(file.getBytes()));
            if (file.getContentType() != null && file.getContentType().contains("image")) {
                fileStorage.setUrl("/v2/file/viewImage/" + fileStorage.getName());
            }

            FileStorage fileStorageSaved = fileStorageDao.save(fileStorage);

            if (fileStorageSaved.getName() == null) {
                throw new MyException("file.error");
            }

            return new FileResponse(fileStorageSaved.getId(), fileStorageSaved.getName(), fileStorageSaved.getUrl(), fileStorageSaved.getType());
        } catch (Exception e) {
            throw new MyException("file.error");
        }
    }

    @Override
    public FileDownloadResponse doDownloadFile(String fileName) {
        if (fileName == null || fileName.length() == 0) {
            throw new MyException("action.invalid");
        }

        FileStorage dbBo = fileStorageDao.findByName(fileName);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }

        FileDownloadResponse fileDownloadResponse = new FileDownloadResponse();
        fileDownloadResponse.setType(dbBo.getType());
        fileDownloadResponse.setData(FileUtil.decompressFile(dbBo.getData()));

        return fileDownloadResponse;
    }

    @Override
    public FileStorage doDelete(FileStorage bo) {
        FileStorage dbBo = fileStorageDao.findById(bo.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }
        fileStorageDao.delete(dbBo);
        return dbBo;
    }

}

package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.dao.SurveyCategoryDao;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SurveyCategoryV2ServiceImpl implements SurveyCategoryV2Service {

    private final SurveyCategoryDao surveyCategoryDao;

    @Override
    public List<SurveyCategory> findAllCategory() {
        return SurveyCategory.toResponseList(surveyCategoryDao.findAllByOrderByPriorityAsc());
    }

    @Override
    public SurveyCategory getSurveyCategory(SurveyCategory bo) {
        if (bo == null) {
            throw new MyException("action.invalid");
        }

        if (bo.getId() == null) {
            throw new MyException("action.invalid");
        }

        return SurveyCategory.toResponse(surveyCategoryDao.findById(bo.getId()).orElse(null));
    }
}

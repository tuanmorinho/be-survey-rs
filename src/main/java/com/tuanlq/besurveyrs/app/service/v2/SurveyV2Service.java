package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.web.v2.survey.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SurveyV2Service {
    List<Survey> findAllSurveyByCategory(SurveyV2Request model);

    SurveyV2Response getSurvey(Survey bo);

    DoSurveyResponse doSurvey(HttpServletRequest request, DoSurveyRequest model);

    DoSurveyResponse redoSurvey(HttpServletRequest request, DoSurveyRequest model);

    DoSurveyResponse getPrevQuestion(HttpServletRequest request, SubmitSurveyAnswerRequest model);

    DoSurveyResponse getNextQuestion(HttpServletRequest request, SubmitSurveyAnswerRequest model);

    DoSurveyResponse submitAnswer(HttpServletRequest request, SubmitSurveyAnswerRequest model);
}

package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.job.CountSurveyActiveJob;
import com.tuanlq.besurveyrs.app.job.CountSurveyTotalJob;
import lombok.RequiredArgsConstructor;
import org.quartz.*;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TriggerJobServiceImpl implements TriggerJobService {

    private final Scheduler scheduler;

    @Override
    public void triggerCountSurveyActiveJob() throws SchedulerException {
        JobKey key = JobKey.jobKey("CountSurveyActiveJob", "CountSurveyActive");
        if (scheduler.checkExists(key)) {
            scheduler.deleteJob(key);
        }
        JobDetail jobDetail = JobBuilder.newJob(CountSurveyActiveJob.class)
                .withIdentity("CountSurveyActiveJob", "CountSurveyActive")
                .storeDurably()
                .build();
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("CountSurveyActiveJob", "CountSurveyActive")
                .forJob("CountSurveyActiveJob", "CountSurveyActive")
                .withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(30))
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public void triggerCountSurveyTotalJob() throws SchedulerException {
        JobKey key = JobKey.jobKey("CountSurveyTotalJob", "CountSurveyTotal");
        if (scheduler.checkExists(key)) {
            scheduler.deleteJob(key);
        }
        JobDetail jobDetail = JobBuilder.newJob(CountSurveyTotalJob.class)
                .withIdentity("CountSurveyTotalJob", "CountSurveyTotal")
                .storeDurably()
                .build();
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("CountSurveyTotalJob", "CountSurveyTotal")
                .forJob("CountSurveyTotalJob", "CountSurveyTotal")
                .withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(32))
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public void triggerCountSurveyQuestionJob() throws SchedulerException {

    }

    @Override
    public void triggerCountSurveyAnswerJob() throws SchedulerException {

    }
}

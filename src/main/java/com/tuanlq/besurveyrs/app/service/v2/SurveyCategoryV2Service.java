package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.bo.SurveyCategory;

import java.util.List;

public interface SurveyCategoryV2Service {
    List<SurveyCategory> findAllCategory();

    SurveyCategory getSurveyCategory(SurveyCategory bo);
}

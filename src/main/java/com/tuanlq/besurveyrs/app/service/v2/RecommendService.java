package com.tuanlq.besurveyrs.app.service.v2;

import com.tuanlq.besurveyrs.app.web.v2.recommend.RecommendRequest;
import com.tuanlq.besurveyrs.app.web.v2.recommend.RecommendResponse;
import jakarta.servlet.http.HttpServletRequest;

public interface RecommendService {
    RecommendResponse getResult(HttpServletRequest request, RecommendRequest model);
}

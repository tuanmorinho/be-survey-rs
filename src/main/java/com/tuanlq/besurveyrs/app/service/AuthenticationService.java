package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.web.authentication.AuthenticationRequest;
import com.tuanlq.besurveyrs.app.web.authentication.AuthenticationResponse;

public interface AuthenticationService {
    AuthenticationResponse register(AuthenticationRequest model);

    AuthenticationResponse login(AuthenticationRequest model);
}

package com.tuanlq.besurveyrs.app.service;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.common.constant.SurveyConstant;
import com.tuanlq.besurveyrs.app.dao.SurveyCategoryDao;
import com.tuanlq.besurveyrs.app.dao.SurveyDao;
import com.tuanlq.besurveyrs.app.web.survey.SurveyRequest;
import com.tuanlq.besurveyrs.app.web.survey.SurveyResponse;
import com.tuanlq.besurveyrs.app.web.survey.SurveySearchRequest;
import com.tuanlq.besurveyrs.infrastructure.core.obj.StatusInfo;
import com.tuanlq.besurveyrs.infrastructure.core.service.AbstractStatusService;
import com.tuanlq.besurveyrs.infrastructure.core.utils.PageUtil;
import com.tuanlq.besurveyrs.infrastructure.core.utils.StringUtils;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SurveyServiceImpl extends AbstractStatusService<Survey> implements SurveyService {

    private final SurveyDao surveyDao;
    private final SurveyCategoryDao surveyCategoryDao;

    @Override
    public StatusInfo statusInfo() {
        List<Map<String, ?>> statusCount = surveyDao.getStatusCount();
        List<Map<String, ?>> trashCount = surveyDao.getTrashCount();
        return new StatusInfo(statusCount, trashCount);
    }

    @Override
    public Page<Survey> findBySearch(SurveySearchRequest model) {
        Pageable pageable = PageUtil.getPageRequest(model);
        return surveyDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if (model.getKeyword() != null && !model.getKeyword().isEmpty()) {
                predicates.add(builder.like(root.get(Survey._name), "%" + model.getKeyword() + "%"));
            }

            if (model.getSurveyCategoryId() != null) {
                predicates.add(builder.equal(root.get(Survey._surveyCategory).get(SurveyCategory._id), model.getSurveyCategoryId()));
            }

            if (model.getIsTrash() != null) {
                predicates.add(builder.equal(root.get(Survey._isTrash), model.getIsTrash()));
            }

            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(Survey._status), model.getStatus()));
            }

            query.orderBy(builder.desc(root.get(Survey._createdDate)), builder.desc(root.get(Survey._id)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public SurveyResponse getSurveyDetail(Integer id) {
        if (id == null) {
            throw new MyException("error.get");
        }

        Survey dbBo = surveyDao.findById(id).orElse(null);
        if (dbBo == null) {
            throw new MyException("error.get");
        }

        SurveyResponse surveyResponse = new SurveyResponse();
        if (dbBo.getSurveyCategory() != null) {
            surveyResponse.setSurveyCategoryId(dbBo.getSurveyCategory().getId());
        }
        surveyResponse.setId(dbBo.getId());
        surveyResponse.setCode(dbBo.getCode());
        surveyResponse.setName(dbBo.getName());
        surveyResponse.setSlug(dbBo.getSlug());
        surveyResponse.setDescription(dbBo.getDescription());
        surveyResponse.setImageThumbnail(dbBo.getImageThumbnail());
        surveyResponse.setImageFeature(dbBo.getImageFeature());
        surveyResponse.setNextStepType(dbBo.getNextStepType());
        surveyResponse.setStatus(dbBo.getStatus());
        surveyResponse.setIsTrash(dbBo.getIsTrash());

        return surveyResponse;
    }

    @Override
    public Survey doCreateSurvey(SurveyRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getName() == null) {
            throw new MyException("action.invalid");
        }

        if (model.getSurveyCategoryId() == null) {
            throw new MyException("action.invalid");
        }
        SurveyCategory dbSurveyCategoryBo = surveyCategoryDao.findById(model.getSurveyCategoryId()).orElse(null);
        if (dbSurveyCategoryBo == null) {
            throw new MyException("action.invalid");
        }

        Survey survey = new Survey();
        survey.setSurveyCategory(dbSurveyCategoryBo);
        String surveyCode = model.getCode() == null ? StringUtils.getRandomCode(8) : model.getCode();
        String slug = model.getSlug() == null ? StringUtils.generateSlug(model.getName()) : model.getSlug();
        survey.setCode(surveyCode);
        survey.setName(model.getName());
        survey.setSlug(slug);
        survey.setDescription(model.getDescription());
        survey.setImageFeature(model.getImageFeature());
        survey.setImageThumbnail(model.getImageThumbnail());
        survey.setNextStepType(model.getNextStepType());
        survey.setIsTrash(Boolean.FALSE);
        survey.setStatus(SurveyConstant.STATUS_INACTIVE);

        surveyDao.save(survey);

        return survey;
    }

    @Override
    public Survey doUpdateSurvey(SurveyRequest model) {
        if (model == null) {
            throw new MyException("action.invalid");
        }

        if (model.getId() == null) {
            throw new MyException("action.invalid");
        }

        Survey dbBo = get(model.getId());

        if (model.getStatus().equals(SurveyConstant.STATUS_ACTIVE)
                && (!model.getNextStepType().equals(dbBo.getNextStepType())
                || !model.getSurveyCategoryId().equals(dbBo.getSurveyCategory().getId()))) {
            throw new MyException("survey.active.edit.error");
        }

        SurveyCategory surveyCategory = surveyCategoryDao.findById(model.getSurveyCategoryId()).orElse(null);
        if (surveyCategory == null) {
            throw new MyException("action.invalid");
        }

        dbBo.setSurveyCategory(surveyCategory);
        dbBo.setName(model.getName());
        dbBo.setDescription(model.getDescription());
        dbBo.setImageThumbnail(model.getImageThumbnail());
        dbBo.setImageFeature(model.getImageFeature());
        dbBo.setNextStepType(model.getNextStepType());

        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public Survey doUpdateStatus(Survey bo) {
        Survey dbBo = surveyDao.findById(bo.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }
        if (dbBo.getIsTrash().equals(Boolean.TRUE)) {
            throw new MyException("action.invalid");
        }
        dbBo.setStatus(bo.getStatus());
        surveyDao.save(dbBo);
        return dbBo;
    }

    @Override
    public Survey doTrash(Survey bo) {
        Survey dbBo = surveyDao.findById(bo.getId()).orElse(null);
        if (dbBo == null) {
            throw new MyException("action.invalid");
        }
        dbBo.setIsTrash(Boolean.TRUE);
        dbBo.setStatus(SurveyConstant.STATUS_INACTIVE);
        surveyDao.save(dbBo);
        return dbBo;
    }
}

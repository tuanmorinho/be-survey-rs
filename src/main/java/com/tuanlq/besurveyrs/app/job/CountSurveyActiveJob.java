package com.tuanlq.besurveyrs.app.job;

import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.dao.SurveyCategoryDao;
import com.tuanlq.besurveyrs.app.dao.SurveyDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;

@RequiredArgsConstructor
public class CountSurveyActiveJob extends QuartzJobBean {

    private final SurveyCategoryDao surveyCategoryDao;
    private final SurveyDao surveyDao;

    @Override
    protected void executeInternal(@NonNull JobExecutionContext jobExecutionContext) {
//        List<SurveyCategory> surveyCategoryList = surveyCategoryDao.findAll();
//        if (surveyCategoryList.size() > 0) {
//            for (SurveyCategory surveyCategory : surveyCategoryList) {
//                surveyCategory.setSurveyActiveCount(surveyDao.countSurveyActiveBySurveyCategoryId(surveyCategory.getId()));
//            }
//        }
//        surveyCategoryDao.saveAll(surveyCategoryList);
    }
}

package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.infrastructure.core.dao.StatusDao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SurveyDao extends StatusDao<Survey> {
    @Query(value = "SELECT ts.* FROM TBL_SURVEY ts \n" +
            "JOIN TBL_SURVEY_CATEGORY tsc ON ts.SURVEY_CATEGORY_ID = tsc.ID \n" +
            "WHERE tsc.ID = :surveyCategoryId", nativeQuery = true)
    List<Survey> findAllBySurveyCategoryId(Integer surveyCategoryId);

    @Query(value = "SELECT COUNT(ts.id) FROM TBL_SURVEY ts \n" +
            "WHERE ts.SURVEY_CATEGORY_ID = :surveyCategoryId AND ts.STATUS = 1 AND ts.IS_TRASH = 0", nativeQuery = true)
    Integer countSurveyActiveBySurveyCategoryId(Integer surveyCategoryId);

    @Query(value = "SELECT COUNT(ts.id) FROM TBL_SURVEY ts \n" +
            "WHERE ts.SURVEY_CATEGORY_ID = :surveyCategoryId AND ts.IS_TRASH = 0", nativeQuery = true)
    Integer countSurveyTotalBySurveyCategoryId(Integer surveyCategoryId);

    @Query(value = "SELECT x.type, x.label, x.description, x.value, IFNULL(y.COUNT, 0) AS count FROM TBL_OBJECT_CATEGORY x\n" +
            "LEFT JOIN (SELECT a.STATUS, COUNT(*) AS COUNT FROM TBL_SURVEY a WHERE a.IS_TRASH = 0 GROUP BY a.STATUS) y\n" +
            "ON x.VALUE = y.STATUS WHERE x.TYPE = 'Survey' and x.PROPERTY = 'status'", nativeQuery = true)
    List<Map<String, ?>> getStatusCount();

    @Query(value = "SELECT x.type, x.label, x.description, x.value, IFNULL(y.COUNT, 0) AS count FROM TBL_OBJECT_CATEGORY x\n" +
            "LEFT JOIN (SELECT a.IS_TRASH, COUNT(*) AS count FROM TBL_SURVEY a GROUP BY a.IS_TRASH) y \n" +
            "ON x.VALUE = y.IS_TRASH WHERE x.TYPE = 'Trash' and x.PROPERTY = 'isTrash'", nativeQuery = true)
    List<Map<String, ?>> getTrashCount();
}

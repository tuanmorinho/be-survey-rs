package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestionStep;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyQuestionStepDao extends Dao<SurveyQuestionStep, Integer> {
    @Query(value = "SELECT tsqt.* FROM TBL_SURVEY_QUESTION_STEP tsqt \n" +
            "WHERE tsqt.SURVEY_QUESTION_ID = :surveyQuestionId", nativeQuery = true)
    List<SurveyQuestionStep> findBySurveyQuestionId(Integer surveyQuestionId);

    SurveyQuestionStep findBySurveyQuestionAndSurveyAnswer(SurveyQuestion surveyQuestion, SurveyAnswer surveyAnswer);
}

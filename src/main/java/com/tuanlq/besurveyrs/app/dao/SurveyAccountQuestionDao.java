package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.SurveyAccount;
import com.tuanlq.besurveyrs.app.bo.SurveyAccountQuestion;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyAccountQuestionDao extends Dao<SurveyAccountQuestion, Integer> {
    @Query(value = "SELECT tsaq1.*\n" +
            "FROM TBL_SURVEY_ACCOUNT_QUESTION tsaq1\n" +
            "WHERE tsaq1.STEP_INDEX = ( \n" +
            "\tSELECT MAX(tsaq2.STEP_INDEX) AS max_step\n" +
            "\tFROM TBL_SURVEY_ACCOUNT_QUESTION tsaq2\n" +
            "\tGROUP BY tsaq2.SURVEY_ACCOUNT_ID\n" +
            "\tHAVING tsaq2.SURVEY_ACCOUNT_ID = :surveyAccountId\n" +
            ") AND SURVEY_ACCOUNT_ID = :surveyAccountId", nativeQuery = true)
    SurveyAccountQuestion findBySurveyAccountIdAndStepIndexMax(Integer surveyAccountId);

    List<SurveyAccountQuestion> findAllBySurveyAccount(SurveyAccount surveyAccount);

    SurveyAccountQuestion findByIdAndSurveyAccountIdAndStepIndex(Integer id, Integer surveyAccountId, Integer stepIndex);

    SurveyAccountQuestion findBySurveyAccountIdAndSurveyQuestionId(Integer surveyAccountId, Integer surveyQuestionId);
}

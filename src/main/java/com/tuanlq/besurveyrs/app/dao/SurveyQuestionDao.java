package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyQuestionDao extends Dao<SurveyQuestion, Integer> {
    SurveyQuestion findBySurveyIdAndIsFirst(Integer surveyId, Boolean isFirst);
}

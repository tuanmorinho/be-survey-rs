package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.Product;
import com.tuanlq.besurveyrs.infrastructure.core.dao.StatusDao;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends StatusDao<Product> {
}

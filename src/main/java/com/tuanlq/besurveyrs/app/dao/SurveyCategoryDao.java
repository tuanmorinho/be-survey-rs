package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyCategoryDao extends Dao<SurveyCategory, Integer> {
    @Query(value = "SELECT MAX(tsc.PRIORITY) FROM TBL_SURVEY_CATEGORY tsc", nativeQuery = true)
    Integer getPriorityMax();

    List<SurveyCategory> findAllByOrderByPriorityAsc();
}

package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.app.bo.FileStorage;
import com.tuanlq.besurveyrs.app.web.file.FileResponse;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileStorageDao extends Dao<FileStorage, Integer> {

    FileStorage findByName(String name);

    FileStorage findByNameAndFileFolder(String name, FileFolder fileFolder);

    @Query(value = "select new com.tuanlq.besurveyrs.app.web.file.FileResponse(fs.id, fs.name, fs.url, fs.type) " +
            "from FileStorage fs " +
            "where fs.fileFolder is null " +
            "order by fs.createdDate desc")
    List<FileResponse> findFileNotInFolder();

    @Query(value = "select new com.tuanlq.besurveyrs.app.web.file.FileResponse(fs.id, fs.name, fs.url, fs.type) " +
            "from FileStorage fs " +
            "where fs.fileFolder = :fileFolder " +
            "order by fs.createdDate desc")
    List<FileResponse> findFileInFolder(FileFolder fileFolder);

}

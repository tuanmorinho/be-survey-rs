package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.SurveyAccountAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyAccountQuestion;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyAccountAnswerDao extends Dao<SurveyAccountAnswer, Integer> {
    SurveyAccountAnswer findBySurveyAccountQuestion(SurveyAccountQuestion surveyAccountQuestion);
}

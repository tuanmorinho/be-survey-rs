package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountDao extends Dao<Account, Integer> {
    @Query("SELECT a FROM Account a LEFT JOIN FETCH a.roles r WHERE a.username = :username")
    Optional<Account> findByUsernameWithRoles(String username);
}

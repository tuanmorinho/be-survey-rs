package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.SurveyAccount;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyAccountDao extends Dao<SurveyAccount, Integer> {
    SurveyAccount findBySurveyIdAndAccountId(Integer surveyId, Integer surveyAccountId);

    SurveyAccount findBySurveyIdAndVisitorUuidAndAccountIsNull(Integer surveyId, String visitorUuid);
}

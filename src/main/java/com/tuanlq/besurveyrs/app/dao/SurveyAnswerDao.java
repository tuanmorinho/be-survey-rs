package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyAnswerDao extends Dao<SurveyAnswer, Integer> {
    @Query(value = "SELECT tsa.* FROM TBL_SURVEY_ANSWER tsa \n" +
            "JOIN TBL_SURVEY_QUESTION tsq ON tsa.SURVEY_QUESTION_ID = tsq.ID \n" +
            "WHERE tsq.ID = :surveyQuestionId", nativeQuery = true)
    List<SurveyAnswer> findAllBySurveyQuestionId(Integer surveyQuestionId);

    @Query(value = "SELECT tsa.* FROM TBL_SURVEY_ANSWER tsa \n" +
            "JOIN TBL_SURVEY_QUESTION tsq ON tsa.SURVEY_QUESTION_ID = tsq.ID \n" +
            "WHERE tsq.ID = :surveyQuestionId \n" +
            "ORDER BY tsa.PRIORITY ASC", nativeQuery = true)
    List<SurveyAnswer> findAllBySurveyQuestionIdSortByDirectionAsc(Integer surveyQuestionId);

    @Query(value = "SELECT MAX(tsa.PRIORITY) FROM TBL_SURVEY_ANSWER tsa WHERE tsa.SURVEY_QUESTION_ID = :surveyQuestionId", nativeQuery = true)
    Integer getMaxPriorityAnswer(Integer surveyQuestionId);

    List<SurveyAnswer> findAllBySurveyQuestionIdOrderByPriorityAsc(Integer questionId);

    List<SurveyAnswer> findAllByIdIn(List<Integer> ids);
}

package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.stereotype.Repository;

@Repository
public interface FileFolderDao extends Dao<FileFolder, Integer> {
}

package com.tuanlq.besurveyrs.app.dao;

import com.tuanlq.besurveyrs.app.bo.Role;
import com.tuanlq.besurveyrs.infrastructure.core.dao.Dao;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends Dao<Role, Integer> {
    Role getByName(String name);
}

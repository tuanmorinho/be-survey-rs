package com.tuanlq.besurveyrs.app.web.v2.survey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoSurveyRequest {
    private Integer surveyId;
}

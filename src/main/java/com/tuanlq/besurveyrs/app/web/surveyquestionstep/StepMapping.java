package com.tuanlq.besurveyrs.app.web.surveyquestionstep;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StepMapping {
    private Integer surveyAnswerId;
    private SurveyAnswer surveyAnswer;
    private Integer surveyQuestionNextId;
    private SurveyQuestion surveyQuestionNext;
}

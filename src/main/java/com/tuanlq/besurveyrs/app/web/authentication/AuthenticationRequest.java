package com.tuanlq.besurveyrs.app.web.authentication;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationRequest {
    private String mobile;
    private String email;
    private String username;
    private String password;
    private String avatarUrl;
    private String fullName;
    private Integer registerType;
}

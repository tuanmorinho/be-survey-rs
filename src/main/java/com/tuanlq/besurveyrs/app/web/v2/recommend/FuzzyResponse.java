package com.tuanlq.besurveyrs.app.web.v2.recommend;

import com.tuanlq.besurveyrs.app.bo.Product;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FuzzyResponse {
    private Product product;
}

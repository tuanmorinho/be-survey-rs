package com.tuanlq.besurveyrs.app.web.file;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileResponse {
    private Integer id;
    private String name;
    private String url;
    private String type;

    public FileResponse(Integer id, String name, String url, String type) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.type = type;
    }
}

package com.tuanlq.besurveyrs.app.web.surveyanswer;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.service.SurveyAnswerService;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractController;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/survey-answer")
@RequiredArgsConstructor
public class SurveyAnswerController extends AbstractController<SurveyAnswer> {

    private final SurveyAnswerService surveyAnswerService;

    @PostMapping("/findBySearch")
    public Page<SurveyAnswer> findBySearch(@RequestBody SurveyAnswerSearchRequest model) {
        return surveyAnswerService.findBySearch(model);
    }

    @GetMapping("/findAllSurveyAnswer/{surveyQuestionId}")
    public List<SurveyAnswer> findAllSurveyAnswer(@PathVariable Integer surveyQuestionId) {
        return surveyAnswerService.findAllSurveyAnswer(surveyQuestionId);
    }

    @GetMapping("/detailSurveyAnswer/{id}")
    public SurveyAnswerResponse getSurveyAnswerDetail(@PathVariable Integer id) {
        return surveyAnswerService.getSurveyAnswerDetail(id);
    }

    @PostMapping("/createSurveyAnswer")
    public SurveyAnswer doCreateSurveyAnswer(@RequestBody SurveyAnswerRequest model) {
        return surveyAnswerService.doCreateSurveyAnswer(model);
    }

    @PostMapping("/updateSurveyAnswer/{id}")
    public SurveyAnswer doUpdateSurveyAnswer(@PathVariable Integer id, @RequestBody SurveyAnswerRequest model) {
        model.setId(id);
        return surveyAnswerService.doUpdateSurveyAnswer(model);
    }

    @PutMapping("/priority")
    public List<SurveyAnswer> priority(@RequestBody List<SurveyAnswer> surveyAnswerList) {
        return surveyAnswerService.doPriority(surveyAnswerList);
    }

    @PostMapping("/validateCreateSurveyAnswer")
    public boolean validateCreateSurveyAnswer(@RequestBody SurveyAnswerRequest model) {
        return surveyAnswerService.validateCreateSurveyAnswer(model);
    }
}

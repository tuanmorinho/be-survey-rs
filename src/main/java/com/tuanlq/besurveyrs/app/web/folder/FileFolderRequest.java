package com.tuanlq.besurveyrs.app.web.folder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileFolderRequest {
    private Integer fileFolderParentId;
    private String name;
}

package com.tuanlq.besurveyrs.app.web.surveycategory;

import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.service.SurveyCategoryService;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractController;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/survey-category")
@RequiredArgsConstructor
public class SurveyCategoryController extends AbstractController<SurveyCategory> {

    private final SurveyCategoryService surveyCategoryService;

    @PostMapping("/findBySearch")
    public Page<SurveyCategory> findBySearch(@RequestBody SurveyCategorySearchRequest model) {
        return surveyCategoryService.findBySearch(model);
    }

    @PutMapping("/priority")
    public List<SurveyCategory> priority(@RequestBody List<SurveyCategory> surveyCategoryList) {
        return surveyCategoryService.doPriority(surveyCategoryList);
    }

}

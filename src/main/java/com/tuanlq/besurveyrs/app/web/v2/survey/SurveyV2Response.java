package com.tuanlq.besurveyrs.app.web.v2.survey;

import com.tuanlq.besurveyrs.app.bo.Survey;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyV2Response extends Survey {
    private Integer surveyCategoryId;
}

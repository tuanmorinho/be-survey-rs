package com.tuanlq.besurveyrs.app.web.v2.survey;

import com.tuanlq.besurveyrs.app.bo.Survey;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyV2Request extends Survey {
    private Integer surveyCategoryId;
}

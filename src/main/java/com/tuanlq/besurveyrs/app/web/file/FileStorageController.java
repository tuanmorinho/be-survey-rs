package com.tuanlq.besurveyrs.app.web.file;

import com.tuanlq.besurveyrs.app.bo.FileStorage;
import com.tuanlq.besurveyrs.app.service.FileStorageService;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractController;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileStorageController extends AbstractController<FileStorage> {

    private final FileStorageService fileStorageService;

    @PostMapping("/upload")
    public FileResponse doUploadFile(@RequestParam(value = "file") MultipartFile file,
                                     @RequestParam(value = "folderId", required = false) Integer folderId) {
        return fileStorageService.doUploadFile(file, folderId);
    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<?> doDownloadFile(@PathVariable String fileName) {
        FileDownloadResponse file = fileStorageService.doDownloadFile(fileName);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf(file.getType()))
                .body(file.getData());
    }

}

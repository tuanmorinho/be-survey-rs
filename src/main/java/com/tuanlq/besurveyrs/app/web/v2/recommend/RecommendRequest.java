package com.tuanlq.besurveyrs.app.web.v2.recommend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecommendRequest {
    private Integer surveyId;
}

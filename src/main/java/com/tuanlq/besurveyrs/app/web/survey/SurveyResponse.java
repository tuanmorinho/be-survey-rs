package com.tuanlq.besurveyrs.app.web.survey;

import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyResponse {
    private Integer id;
    private Integer surveyCategoryId;
    private String code;
    private String name;
    private String slug;
    private String description;
    private String imageThumbnail;
    private String imageFeature;
    private Integer nextStepType;
    private Integer status;
    private Boolean isTrash;
}

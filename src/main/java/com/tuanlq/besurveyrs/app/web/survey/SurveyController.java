package com.tuanlq.besurveyrs.app.web.survey;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.service.SurveyService;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractStatusController;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/survey")
@RequiredArgsConstructor
public class SurveyController extends AbstractStatusController<Survey> {

    private final SurveyService surveyService;

    @PostMapping("/findBySearch")
    public Page<Survey> findBySearch(@RequestBody SurveySearchRequest model) {
        return surveyService.findBySearch(model);
    }

    @GetMapping("/detailSurvey/{id}")
    public SurveyResponse getSurveyDetail(@PathVariable Integer id) {
        return surveyService.getSurveyDetail(id);
    }

    @PostMapping("/createSurvey")
    public Survey doCreateSurvey(@RequestBody SurveyRequest model) {
        return surveyService.doCreateSurvey(model);
    }

    @PostMapping("/updateSurvey/{id}")
    public Survey doUpdateSurvey(@PathVariable Integer id, @RequestBody SurveyRequest model) {
        model.setId(id);
        return surveyService.doUpdateSurvey(model);
    }
}

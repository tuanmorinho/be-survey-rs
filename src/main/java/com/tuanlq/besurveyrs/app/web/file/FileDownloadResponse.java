package com.tuanlq.besurveyrs.app.web.file;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileDownloadResponse {
    private String type;
    private byte[] data;
}

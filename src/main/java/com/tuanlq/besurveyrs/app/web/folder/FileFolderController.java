package com.tuanlq.besurveyrs.app.web.folder;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.app.service.FileFolderService;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractController;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/folder")
@RequiredArgsConstructor
public class FileFolderController extends AbstractController<FileFolder> {

    private final FileFolderService fileFolderService;

    @PostMapping("/view")
    public FileFolderListResponse viewFolder(@RequestBody FileFolder bo) {
        return fileFolderService.viewFolder(bo);
    }

    @PostMapping("/create")
    public FileFolder doInsertFolder(@RequestBody FileFolderRequest model) {
        return fileFolderService.doInsertFolder(model);
    }
}

package com.tuanlq.besurveyrs.app.web.triggerjob;

import com.tuanlq.besurveyrs.app.service.TriggerJobService;
import lombok.RequiredArgsConstructor;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/job")
@RequiredArgsConstructor
public class TriggerJobController {

    private final TriggerJobService triggerJobService;

    @PostMapping("/trigg")
    public void triggerCountSurveyJob() throws SchedulerException {
        triggerJobService.triggerCountSurveyActiveJob();
        triggerJobService.triggerCountSurveyTotalJob();
        triggerJobService.triggerCountSurveyQuestionJob();
        triggerJobService.triggerCountSurveyAnswerJob();
    }
}

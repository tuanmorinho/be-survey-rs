package com.tuanlq.besurveyrs.app.web.v2.survey;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyCategory;
import com.tuanlq.besurveyrs.app.service.v2.SurveyCategoryV2Service;
import com.tuanlq.besurveyrs.app.service.v2.SurveyV2Service;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v2/survey")
@RequiredArgsConstructor
public class SurveyV2Controller {

    private final SurveyV2Service surveyV2Service;
    private final SurveyCategoryV2Service surveyCategoryV2Service;

    @GetMapping("/findAllCategory")
    public List<SurveyCategory> findAllCategory() {
        return surveyCategoryV2Service.findAllCategory();
    }

    @PostMapping("/findAllSurvey")
    public List<Survey> findAllSurveyByCategory(@RequestBody SurveyV2Request model) {
        return surveyV2Service.findAllSurveyByCategory(model);
    }

    @PostMapping("/getCategory")
    public SurveyCategory getSurveyCategory(@RequestBody SurveyCategory bo) {
        return surveyCategoryV2Service.getSurveyCategory(bo);
    }

    @PostMapping("/getSurvey")
    public SurveyV2Response getSurvey(@RequestBody Survey bo) {
        return surveyV2Service.getSurvey(bo);
    }

    @PostMapping("/doSurvey")
    public DoSurveyResponse doSurvey(HttpServletRequest request, @RequestBody DoSurveyRequest model) {
        return surveyV2Service.doSurvey(request, model);
    }

    @PostMapping("/getPrevQuestion")
    public DoSurveyResponse getPrevQuestion(HttpServletRequest request, @RequestBody SubmitSurveyAnswerRequest model) {
        return surveyV2Service.getPrevQuestion(request, model);
    }

    @PostMapping("/getNextQuestion")
    public DoSurveyResponse getNextQuestion(HttpServletRequest request, @RequestBody SubmitSurveyAnswerRequest model) {
        return surveyV2Service.getNextQuestion(request, model);
    }

    @PostMapping("/submitAnswer")
    public DoSurveyResponse submitAnswer(HttpServletRequest request, @RequestBody SubmitSurveyAnswerRequest model) {
        return surveyV2Service.submitAnswer(request, model);
    }

    @PostMapping("/redoSurvey")
    public DoSurveyResponse redoSurvey(HttpServletRequest request, @RequestBody DoSurveyRequest model) {
        return surveyV2Service.redoSurvey(request, model);
    }
}

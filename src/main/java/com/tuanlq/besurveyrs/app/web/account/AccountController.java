package com.tuanlq.besurveyrs.app.web.account;

import com.tuanlq.besurveyrs.app.bo.Account;
import com.tuanlq.besurveyrs.app.service.AccountService;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractController;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController extends AbstractController<Account> {

    private final AccountService accountService;
}

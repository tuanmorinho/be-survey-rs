package com.tuanlq.besurveyrs.app.web.survey;

import com.tuanlq.besurveyrs.infrastructure.core.obj.MyPage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveySearchRequest extends MyPage {
    private String keyword;
    private Integer surveyCategoryId;
    private Boolean isTrash;
    private Integer status;
}

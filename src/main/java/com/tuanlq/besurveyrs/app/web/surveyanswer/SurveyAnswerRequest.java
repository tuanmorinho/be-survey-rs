package com.tuanlq.besurveyrs.app.web.surveyanswer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyAnswerRequest {
    private Integer id;
    private Integer surveyId;
    private Integer surveyQuestionId;
    private String name;
    private String description;
    private String imageThumbnail;
    private String imageFeature;
    private Integer displayType;
    private Integer priority;
    private Integer demandScore;
}

package com.tuanlq.besurveyrs.app.web.v2.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountV2Response {
    private String token;
}

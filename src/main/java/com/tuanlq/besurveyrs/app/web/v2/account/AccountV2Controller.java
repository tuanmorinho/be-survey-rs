package com.tuanlq.besurveyrs.app.web.v2.account;

import com.tuanlq.besurveyrs.app.common.constant.AccountConstant;
import com.tuanlq.besurveyrs.app.service.v2.AccountV2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v2/account")
@RequiredArgsConstructor
public class AccountV2Controller {

    private final AccountV2Service accountV2Service;

    @PostMapping("/register")
    public AccountV2Response register(@RequestBody AccountV2Request model) {
        model.setRegisterType(AccountConstant.REGISTER_TYPE_USER);
        return accountV2Service.register(model);
    }

    @PostMapping("/login")
    public AccountV2Response login(@RequestBody AccountV2Request model) {
        model.setRegisterType(AccountConstant.REGISTER_TYPE_USER);
        return accountV2Service.login(model);
    }
}

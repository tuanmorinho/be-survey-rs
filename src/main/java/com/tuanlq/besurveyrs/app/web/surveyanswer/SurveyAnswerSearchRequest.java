package com.tuanlq.besurveyrs.app.web.surveyanswer;

import com.tuanlq.besurveyrs.infrastructure.core.obj.MyPage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyAnswerSearchRequest extends MyPage {
    private Integer surveyId;
    private Integer surveyQuestionId;
}

package com.tuanlq.besurveyrs.app.web.v2.survey;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SubmitSurveyAnswerRequest {
    private Integer surveyAccountId;
    private Integer surveyId;
    private Integer questionId;
    private Integer stepIndex;
    private List<Integer> answerIdList;
    private Integer answerDemandScore;
    private String answerText;
    private String answerImageUpload1;
    private String answerImageUpload2;
    private String answerImageUpload3;
    private String answerImageUpload4;
    private String answerImageUpload5;
}

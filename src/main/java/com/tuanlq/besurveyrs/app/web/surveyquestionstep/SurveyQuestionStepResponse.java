package com.tuanlq.besurveyrs.app.web.surveyquestionstep;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SurveyQuestionStepResponse {
    private Integer surveyQuestionId;
    private SurveyQuestion surveyQuestion;
    private List<SurveyAnswer> surveyAnswer;
    private List<StepMapping> step;
}

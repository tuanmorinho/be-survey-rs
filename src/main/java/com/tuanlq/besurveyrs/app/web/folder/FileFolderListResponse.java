package com.tuanlq.besurveyrs.app.web.folder;

import com.tuanlq.besurveyrs.app.bo.FileFolder;
import com.tuanlq.besurveyrs.app.web.file.FileResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FileFolderListResponse {
    private List<FileFolder> folderList;
    private List<FileResponse> fileList;
}

package com.tuanlq.besurveyrs.app.web.authentication;

import com.tuanlq.besurveyrs.app.common.constant.AccountConstant;
import com.tuanlq.besurveyrs.app.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public AuthenticationResponse register(@RequestBody AuthenticationRequest model) {
        model.setRegisterType(AccountConstant.REGISTER_TYPE_ADMIN);
        return authenticationService.register(model);
    }

    @PostMapping("/login")
    public AuthenticationResponse login(@RequestBody AuthenticationRequest model) {
        model.setRegisterType(AccountConstant.REGISTER_TYPE_ADMIN);
        return authenticationService.login(model);
    }
}

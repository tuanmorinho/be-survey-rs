package com.tuanlq.besurveyrs.app.web.v2.file;

import com.tuanlq.besurveyrs.app.service.v2.FileStorageV2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v2/file")
@RequiredArgsConstructor
public class FileStorageV2Controller {

    private final FileStorageV2Service fileStorageV2Service;

    @GetMapping("/viewImage/{fileName}")
    public ResponseEntity<?> viewImage(@PathVariable String fileName) {
        ImageFileResponse file = fileStorageV2Service.doViewImage(fileName);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf(file.getType()))
                .body(file.getData());
    }

}

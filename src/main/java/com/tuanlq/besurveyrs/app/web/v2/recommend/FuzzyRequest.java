package com.tuanlq.besurveyrs.app.web.v2.recommend;

import com.tuanlq.besurveyrs.app.bo.Product;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FuzzyRequest {
    private double demandScore;
    private List<Product> productList;
}

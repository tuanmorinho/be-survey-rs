package com.tuanlq.besurveyrs.app.web.surveycategory;

import com.tuanlq.besurveyrs.infrastructure.core.obj.MyPage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyCategorySearchRequest extends MyPage {
    private String keyword;
}

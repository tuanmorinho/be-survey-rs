package com.tuanlq.besurveyrs.app.web.surveyquestion;

import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import com.tuanlq.besurveyrs.app.service.SurveyQuestionService;
import com.tuanlq.besurveyrs.infrastructure.core.controller.AbstractController;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/survey-question")
@RequiredArgsConstructor
public class SurveyQuestionController extends AbstractController<SurveyQuestion> {

    private final SurveyQuestionService surveyQuestionService;

    @PostMapping("/findBySearch")
    public Page<SurveyQuestion> findBySearch(@RequestBody SurveyQuestionSearchRequest model) {
        return surveyQuestionService.findBySearch(model);
    }

    @GetMapping("/detailSurveyQuestion/{id}")
    public SurveyQuestionResponse getSurveyQuestionDetail(@PathVariable Integer id) {
        return surveyQuestionService.getSurveyQuestionDetail(id);
    }

    @PostMapping("/createSurveyQuestion")
    public SurveyQuestion doCreateSurveyQuestion(@RequestBody SurveyQuestionRequest model) {
        return surveyQuestionService.doCreateSurveyQuestion(model);
    }

    @PostMapping("/updateSurveyQuestion/{id}")
    public SurveyQuestion doUpdateSurveyQuestion(@PathVariable Integer id, @RequestBody SurveyQuestionRequest model) {
        model.setId(id);
        return surveyQuestionService.doUpdateSurveyQuestion(model);
    }

    @PostMapping("/deleteSurveyQuestion/{id}")
    public SurveyQuestion doDeleteSurveyQuestion(@PathVariable Integer id, @RequestBody SurveyQuestionRequest model) {
        model.setId(id);
        return surveyQuestionService.doDeleteSurveyQuestion(model);
    }

    @PostMapping("/validateCreateSurveyQuestion")
    public boolean validateCreateSurveyQuestion(@RequestBody SurveyQuestionRequest model) {
        return surveyQuestionService.validateCreateSurveyQuestion(model);
    }
}

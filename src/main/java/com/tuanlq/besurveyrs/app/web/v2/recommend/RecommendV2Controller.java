package com.tuanlq.besurveyrs.app.web.v2.recommend;

import com.tuanlq.besurveyrs.app.service.v2.RecommendService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v2/recommend")
@RequiredArgsConstructor
public class RecommendV2Controller {

    private final RecommendService recommendService;

    @PostMapping("/getResult")
    public RecommendResponse getResult(HttpServletRequest request, @RequestBody RecommendRequest model) {
        return recommendService.getResult(request, model);
    }


}

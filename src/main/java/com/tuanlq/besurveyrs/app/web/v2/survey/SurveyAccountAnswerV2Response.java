package com.tuanlq.besurveyrs.app.web.v2.survey;

import com.tuanlq.besurveyrs.app.bo.SurveyAccountAnswer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyAccountAnswerV2Response extends SurveyAccountAnswer {
    private Integer surveyAnswerId;
    private Integer answerDemandScore;
    private String answerText;
    private String answerImageUpload1;
    private String answerImageUpload2;
    private String answerImageUpload3;
    private String answerImageUpload4;
    private String answerImageUpload5;
    private Integer answerDisplayType;
}

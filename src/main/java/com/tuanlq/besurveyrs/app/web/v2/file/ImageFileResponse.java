package com.tuanlq.besurveyrs.app.web.v2.file;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageFileResponse {
    private String type;
    private byte[] data;
}

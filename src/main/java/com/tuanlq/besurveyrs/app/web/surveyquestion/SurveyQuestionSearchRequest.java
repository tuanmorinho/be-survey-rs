package com.tuanlq.besurveyrs.app.web.surveyquestion;

import com.tuanlq.besurveyrs.infrastructure.core.obj.MyPage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyQuestionSearchRequest extends MyPage {
    private Integer surveyId;
}

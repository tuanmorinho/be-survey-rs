package com.tuanlq.besurveyrs.app.web.v2.survey;

import com.tuanlq.besurveyrs.app.bo.SurveyAnswer;
import com.tuanlq.besurveyrs.app.bo.SurveyQuestion;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DoSurveyQuestionResponse extends SurveyQuestion {
    private Integer surveyId;
    private List<SurveyAnswer> surveyAnswerList;
    private List<SurveyAccountAnswerV2Response> surveyAccountAnswerList;
}

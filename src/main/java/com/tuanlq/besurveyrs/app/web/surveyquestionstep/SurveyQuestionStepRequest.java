package com.tuanlq.besurveyrs.app.web.surveyquestionstep;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SurveyQuestionStepRequest {
    private Integer surveyQuestionId;
    private List<StepMapping> step;
}

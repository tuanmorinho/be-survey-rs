package com.tuanlq.besurveyrs.app.web.v2.survey;

import com.tuanlq.besurveyrs.app.bo.Survey;
import com.tuanlq.besurveyrs.app.bo.SurveyAccount;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoSurveyResponse {
    private Integer questionStep;
    private SurveyAccount surveyAccount;
    private Survey survey;
    private DoSurveyQuestionResponse doSurveyQuestionResponse;
    private boolean isDone;
}

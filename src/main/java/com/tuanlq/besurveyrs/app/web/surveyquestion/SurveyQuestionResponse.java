package com.tuanlq.besurveyrs.app.web.surveyquestion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyQuestionResponse {
    private Integer id;
    private Integer surveyId;
    private String name;
    private String description;
    private String imageThumbnail;
    private String imageFeature;
    private Integer answerDisplayType;
    private Integer answerSelectType;
    private Boolean isAnswerRequired;
    private Boolean isFirst;
}

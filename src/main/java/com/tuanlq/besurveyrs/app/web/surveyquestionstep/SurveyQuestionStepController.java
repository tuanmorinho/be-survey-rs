package com.tuanlq.besurveyrs.app.web.surveyquestionstep;

import com.tuanlq.besurveyrs.app.service.SurveyQuestionStepService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/survey-question-step")
@RequiredArgsConstructor
public class SurveyQuestionStepController {

    private final SurveyQuestionStepService surveyQuestionStepService;

    @PostMapping("/mapping-step")
    public ResponseEntity<?> doMappingStep(@RequestBody List<SurveyQuestionStepRequest> model) {
        return surveyQuestionStepService.doMappingStep(model);
    }

    @GetMapping("/detail/{surveyId}")
    public List<SurveyQuestionStepResponse> getMappingDetail(@PathVariable Integer surveyId) {
        return surveyQuestionStepService.getMappingDetail(surveyId);
    }
}

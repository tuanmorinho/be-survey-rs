package com.tuanlq.besurveyrs.app.web.v2.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountV2Request {
    private String mobile;
    private String email;
    private String username;
    private String password;
    private String avatarUrl;
    private String fullName;
    private Integer registerType;
}

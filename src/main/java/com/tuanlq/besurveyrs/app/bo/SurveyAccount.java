package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_ACCOUNT")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurveyAccount extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    @JsonIgnore
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    @JsonIgnore
    private Account account;

    @Column(name = "VISITOR_UUID", nullable = false)
    private String visitorUuid;

    @Column(name = "ACTION_STATUS")
    private Integer actionStatus;

    @Column(name = "ACTION_DATE")
    private Instant actionDate;

    @Override
    public boolean equalsData(Object object) {
        SurveyAccount bo = (SurveyAccount) object;
        if (!BeanUtils.nullSafeEquals(survey.getId(), bo.survey.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(account.getId(), bo.account.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(visitorUuid, bo.visitorUuid)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(actionStatus, bo.actionStatus)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(actionDate, bo.actionDate);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof SurveyAccount bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        survey = bo.survey;
        account = bo.account;
        visitorUuid = bo.visitorUuid;
        actionStatus = bo.actionStatus;
        actionDate = bo.actionDate;
    }
}

package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_QUESTION_STEP")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurveyQuestionStep extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_QUESTION_ID")
    @JsonIgnore
    private SurveyQuestion surveyQuestion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ANSWER_ID")
    @JsonIgnore
    private SurveyAnswer surveyAnswer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_QUESTION_NEXT_ID")
    @JsonIgnore
    private SurveyQuestion surveyQuestionNext;

    public static final String _surveyQuestion = "surveyQuestion";
    public static final String _surveyAnswer = "surveyAnswer";
    public static final String _surveyQuestionNext = "surveyQuestionNext";

    @Override
    public boolean equalsData(Object object) {
        return false;
    }
}

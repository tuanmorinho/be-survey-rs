package com.tuanlq.besurveyrs.app.bo;

import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.core.obj.PriorityObject;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_CATEGORY")
public class SurveyCategory extends AbstractBo implements Bo<Integer>, PriorityObject {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @Column(name = "IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String imageThumbnail;

    @Column(name = "IMAGE_FEATURE", columnDefinition = "TEXT")
    private String imageFeature;

    @Column(name = "PRIORITY")
    private Integer priority;

    public SurveyCategory() {
    }

    public SurveyCategory(Integer id) {
        this.id = id;
    }

    public static final String _name = "name";
    public static final String _description = "description";
    public static final String _imageThumbnail = "imageThumbnail";
    public static final String _imageFeature = "imageFeature";
    public static final String _priority = "priority";

    @Override

    public boolean equalsData(Object object) {
        SurveyCategory bo = (SurveyCategory) object;
        if (!BeanUtils.nullSafeEquals(name, bo.name)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(description, bo.description)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageThumbnail, bo.imageThumbnail)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageFeature, bo.imageFeature)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(priority, bo.priority);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof SurveyCategory bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        name = bo.name;
        description = bo.description;
        imageThumbnail = bo.imageThumbnail;
        imageFeature = bo.imageFeature;
        priority = bo.priority;
    }

    public static SurveyCategory toResponse(SurveyCategory bo) {
        if (bo == null) {
            return null;
        }
        SurveyCategory boClone = BeanUtils.clone(bo);
        boClone.setCreatedBy(null);
        boClone.setCreatedDate(null);
        boClone.setModifiedBy(null);
        boClone.setModifiedDate(null);
        return boClone;
    }

    public static List<SurveyCategory> toResponseList(List<SurveyCategory> boList) {
        if (boList == null || boList.isEmpty()) {
            return boList;
        }
        List<SurveyCategory> boListClone = new ArrayList<>();
        for (SurveyCategory item : boList) {
            boListClone.add(toResponse(item));
        }
        return boListClone;
    }
}

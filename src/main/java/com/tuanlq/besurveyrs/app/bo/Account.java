package com.tuanlq.besurveyrs.app.bo;

import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "TBL_ACCOUNT", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"MOBILE"}),
        @UniqueConstraint(columnNames = {"EMAIL"}),
        @UniqueConstraint(columnNames = {"USERNAME"})
})
public class Account extends AbstractBo implements Bo<Integer>, UserDetails {

    @ManyToMany
    @JoinTable(
            name = "JTBL_ACCOUNT_ROLE",
            joinColumns = {@JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Role> roles = new HashSet<>();

    /*
     * For User
     * */
    @Column(name = "MOBILE")
    private String mobile;
    /*
     * For User
     * */
    @Column(name = "EMAIL")
    private String email;

    /*
     * For Admin User
     * */
    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "FULLNAME", nullable = false)
    private String fullName;

    @Column(name = "AVATAR_URL", columnDefinition = "TEXT")
    private String avatarUrl;

    @Column(name = "REGISTER_TYPE")
    private Integer registerType;

    public Account() {
    }

    public Account(Integer id) {
        this.id = id;
    }

    public static final String _mobile = "mobile";
    public static final String _email = "email";
    public static final String _username = "username";
    public static final String _password = "password";
    public static final String _fullName = "fullName";
    public static final String _avatarUrl = "avatarUrl";
    public static final String _registerType = "registerType";

    @Override
    public boolean equalsData(Object object) {
        Account bo = (Account) object;
        if (!BeanUtils.nullSafeEquals(roles, bo.roles)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(mobile, bo.mobile)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(email, bo.email)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(username, bo.username)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(password, bo.password)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(fullName, bo.fullName)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(avatarUrl, bo.avatarUrl)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(registerType, bo.registerType);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof Account bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        roles = bo.roles;
        mobile = bo.mobile;
        email = bo.email;
        username = bo.username;
        password = bo.password;
        fullName = bo.fullName;
        avatarUrl = bo.avatarUrl;
        registerType = bo.registerType;
    }

    public static Account toResponse(Account bo) {
        if (bo == null) {
            return null;
        }
        Account boClone = BeanUtils.clone(bo);
        boClone.setId(null);
        boClone.setPassword(null);
        boClone.setRegisterType(null);
        boClone.setRoles(null);
        boClone.setCreatedBy(null);
        boClone.setCreatedDate(null);
        boClone.setModifiedBy(null);
        boClone.setModifiedDate(null);
        return boClone;
    }

    public static List<Account> toResponseList(List<Account> boList) {
        if (boList == null || boList.size() == 0) {
            return boList;
        }
        List<Account> boListClone = new ArrayList<>();
        for (Account item : boList) {
            boListClone.add(toResponse(item));
        }
        return boListClone;
    }

    public void addRole(Role role) {
        roles.add(role);
    }

    public void resetRole() {
        roles.clear();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map((role) -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toSet());
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

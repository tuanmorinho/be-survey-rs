package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractStatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Survey extends AbstractStatusBo implements StatusBo {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_CATEGORY_ID")
    @JsonIgnore
    private SurveyCategory surveyCategory;

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "SLUG")
    private String slug;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @Column(name = "IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String imageThumbnail;

    @Column(name = "IMAGE_FEATURE", columnDefinition = "TEXT")
    private String imageFeature;

    @Column(name = "NEXT_STEP_TYPE")
    private Integer nextStepType;

    public Survey() {
    }

    public Survey(Integer id) {
        this.id = id;
    }

    public static final String _surveyCategory = "surveyCategory";
    public static final String _code = "code";
    public static final String _name = "name";
    public static final String _slug = "slug";
    public static final String _description = "description";
    public static final String _imageThumbnail = "imageThumbnail";
    public static final String _imageFeature = "imageFeature";
    public static final String _nextStepType = "nextStepType";

    @Override
    public boolean equalsData(Object object) {
        Survey bo = (Survey) object;
        if (!BeanUtils.nullSafeEquals(surveyCategory.getId(), bo.surveyCategory.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(code, bo.code)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(name, bo.name)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(slug, bo.slug)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(description, bo.description)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageThumbnail, bo.imageThumbnail)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageFeature, bo.imageFeature)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(nextStepType, bo.nextStepType);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof Survey bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        surveyCategory = bo.surveyCategory;
        code = bo.code;
        name = bo.name;
        slug = bo.slug;
        description = bo.description;
        imageThumbnail = bo.imageThumbnail;
        imageFeature = bo.imageFeature;
        nextStepType = bo.nextStepType;
    }
}

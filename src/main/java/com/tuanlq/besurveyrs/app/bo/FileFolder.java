package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_FILE_FOLDER")
public class FileFolder extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_FOLDER_PARENT_ID")
    @JsonIgnore
    private FileFolder fileFolder;

    @Column(name = "NAME")
    private String name;

    public FileFolder() {
    }

    public FileFolder(Integer id) {
        this.id = id;
    }

    public static final String _fileFolder = "fileFolder";
    public static final String _name = "name";

    @Override
    public boolean equalsData(Object object) {
        return false;
    }
}

package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractStatusBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.StatusBo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_PRODUCT")
public class Product extends AbstractStatusBo implements StatusBo {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_CATEGORY_ID")
    @JsonIgnore
    private SurveyCategory surveyCategory;

    @Column(name = "NAME")
    private String name;

    @Column(name = "URL")
    private String url;

    @Column(name = "PRICE")
    private Integer price;

    @Column(name = "DESCRIPTION_SUMMARY", columnDefinition = "TEXT COMMENT 'Mô tả ngắn'")
    private String descriptionSummary;

    @Column(name = "DESCRIPTION_USAGE", columnDefinition = "TEXT COMMENT 'Hướng dẫn sử dụng'")
    private String descriptionUsage;

    @Column(name = "DESCRIPTION_ELEMENT", columnDefinition = "TEXT COMMENT 'Thành phần sản phẩm'")
    private String descriptionElement;

    @Column(name = "DESCRIPTION_FULL", columnDefinition = "TEXT COMMENT 'Mô tả đầy đủ'")
    private String descriptionFull;

    @Column(name = "IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String imageThumbnail;

    public static final String _surveyCategory = "surveyCategory";

    @Override
    public boolean equalsData(Object object) {
        return false;
    }
}

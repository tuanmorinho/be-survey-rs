package com.tuanlq.besurveyrs.app.bo;

import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TBL_ROLE")
public class Role extends AbstractBo implements Bo<Integer> {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    public Role() {
    }

    public Role(Integer id) {
        this.id = id;
    }

    public static final String _name = "name";
    public static final String _description = "description";

    @Override
    public boolean equalsData(Object object) {
        Role bo = (Role) object;
        if (!BeanUtils.nullSafeEquals(name, bo.name)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(description, bo.description);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof Role bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        name = bo.name;
        description = bo.description;
    }

    public static Role toResponse(Role bo) {
        if (bo == null) {
            return null;
        }
        Role boClone = BeanUtils.clone(bo);
        boClone.setCreatedBy(null);
        boClone.setCreatedDate(null);
        boClone.setModifiedBy(null);
        boClone.setModifiedDate(null);
        return boClone;
    }

    public static List<Role> toResponseList(List<Role> boList) {
        if (boList == null || boList.size() == 0) {
            return boList;
        }
        List<Role> boListClone = new ArrayList<>();
        for (Role item : boList) {
            boListClone.add(toResponse(item));
        }
        return boListClone;
    }
}

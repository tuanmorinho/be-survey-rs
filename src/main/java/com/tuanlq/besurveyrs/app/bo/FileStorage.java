package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_FILE_STORAGE", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"NAME", "NO", "TYPE", "URL", "FILE_FOLDER_ID"})
})
public class FileStorage extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_FOLDER_ID")
    @JsonIgnore
    private FileFolder fileFolder;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NO")
    private Integer no;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "URL")
    private String url;

    @Lob
    @Column(name = "DATA", length = 20971520)
    private byte[] data;

    public FileStorage() {
    }

    public FileStorage(Integer id) {
        this.id = id;
    }

    public static final String _fileFolder = "fileFolder";
    public static final String _name = "name";
    public static final String _no = "no";
    public static final String _type = "type";
    public static final String _url = "url";

    @Override
    public boolean equalsData(Object object) {
        return false;
    }
}

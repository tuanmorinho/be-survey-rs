package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_QUESTION")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurveyQuestion extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    @JsonIgnore
    private Survey survey;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @Column(name = "IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String imageThumbnail;

    @Column(name = "IMAGE_FEATURE", columnDefinition = "TEXT")
    private String imageFeature;

    @Column(name = "ANSWER_DISPLAY_TYPE")
    private Integer answerDisplayType;

    @Column(name = "ANSWER_SELECT_TYPE")
    private Integer answerSelectType;

    @Column(name = "IS_ANSWER_REQUIRED")
    private Boolean isAnswerRequired;

    @Column(name = "IS_FIRST")
    private Boolean isFirst;

    public SurveyQuestion() {
    }

    public SurveyQuestion(Integer id) {
        this.id = id;
    }

    public static final String _survey = "survey";
    public static final String _name = "name";
    public static final String _description = "description";
    public static final String _imageThumbnail = "imageThumbnail";
    public static final String _imageFeature = "imageFeature";
    public static final String _answerDisplayType = "answerDisplayType";
    public static final String _answerSelectType = "answerSelectType";
    public static final String _isAnswerRequired = "isAnswerRequired";
    public static final String _isFirst = "isFirst";

    @Override
    public boolean equalsData(Object object) {
        SurveyQuestion bo = (SurveyQuestion) object;
        if (!BeanUtils.nullSafeEquals(survey.getId(), bo.survey.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(name, bo.name)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(description, bo.description)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageThumbnail, bo.imageThumbnail)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageFeature, bo.imageFeature)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerDisplayType, bo.answerDisplayType)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerSelectType, bo.answerSelectType)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(isAnswerRequired, bo.isAnswerRequired)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(isFirst, bo.isFirst);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof SurveyQuestion bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        survey = bo.survey;
        name = bo.name;
        description = bo.description;
        imageThumbnail = bo.imageThumbnail;
        imageFeature = bo.imageFeature;
        answerDisplayType = bo.answerDisplayType;
        answerSelectType = bo.answerSelectType;
        isAnswerRequired = bo.isAnswerRequired;
        isFirst = bo.isFirst;
    }
}

package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_ACCOUNT_ANSWER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurveyAccountAnswer extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ACCOUNT_QUESTION_ID")
    @JsonIgnore
    private SurveyAccountQuestion surveyAccountQuestion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ANSWER_ID")
    @JsonIgnore
    private SurveyAnswer surveyAnswer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    @JsonIgnore
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    @JsonIgnore
    private Account account;

    @Column(name = "ANSWER_DEMAND_SCORE")
    private Integer answerDemandScore;

    @Column(name = "ANSWER_TEXT", columnDefinition = "TEXT")
    private String answerText;

    @Column(name = "ANSWER_IMAGE_UPLOAD_1", columnDefinition = "TEXT")
    private String answerImageUpload1;

    @Column(name = "ANSWER_IMAGE_UPLOAD_2", columnDefinition = "TEXT")
    private String answerImageUpload2;

    @Column(name = "ANSWER_IMAGE_UPLOAD_3", columnDefinition = "TEXT")
    private String answerImageUpload3;

    @Column(name = "ANSWER_IMAGE_UPLOAD_4", columnDefinition = "TEXT")
    private String answerImageUpload4;

    @Column(name = "ANSWER_IMAGE_UPLOAD_5", columnDefinition = "TEXT")
    private String answerImageUpload5;

    @Column(name = "ANSWER_NAME", nullable = false)
    private String answerName;

    @Column(name = "ANSWER_DESCRIPTION", columnDefinition = "TEXT")
    private String answerDescription;

    @Column(name = "ANSWER_IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String answerImageThumbnail;

    @Column(name = "ANSWER_IMAGE_FEATURE", columnDefinition = "TEXT")
    private String answerImageFeature;

    @Column(name = "ANSWER_DISPLAY_TYPE")
    private Integer answerDisplayType;

    @Column(name = "ANSWER_PRIORITY")
    private Integer answerPriority;

    public static final String _surveyAccountQuestion = "surveyAccountQuestion";
    public static final String _surveyAnswer = "surveyAnswer";
    public static final String _survey = "survey";
    public static final String _account = "account";
    public static final String _answerDemandScore = "answerDemandScore";
    public static final String _answerDisplayType = "answerDisplayType";
    public static final String _answerPriority = "answerPriority";

    @Override
    public boolean equalsData(Object object) {
        SurveyAccountAnswer bo = (SurveyAccountAnswer) object;
        if (!BeanUtils.nullSafeEquals(surveyAccountQuestion.getId(), bo.surveyAccountQuestion.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(surveyAnswer.getId(), bo.surveyAnswer.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(survey.getId(), bo.survey.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(account.getId(), bo.account.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerDemandScore, bo.answerDemandScore)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerText, bo.answerText)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageUpload1, bo.answerImageUpload1)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageUpload2, bo.answerImageUpload2)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageUpload3, bo.answerImageUpload3)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageUpload4, bo.answerImageUpload4)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageUpload5, bo.answerImageUpload5)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerName, bo.answerName)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerDescription, bo.answerDescription)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageThumbnail, bo.answerImageThumbnail)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerImageFeature, bo.answerImageFeature)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(answerDisplayType, bo.answerDisplayType)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(answerPriority, bo.answerPriority);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof SurveyAccountAnswer bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        surveyAccountQuestion = bo.surveyAccountQuestion;
        surveyAnswer = bo.surveyAnswer;
        survey = bo.survey;
        account = bo.account;
        answerDemandScore = bo.answerDemandScore;
        answerText = bo.answerText;
        answerImageUpload1 = bo.answerImageUpload1;
        answerImageUpload2 = bo.answerImageUpload2;
        answerImageUpload3 = bo.answerImageUpload3;
        answerImageUpload4 = bo.answerImageUpload4;
        answerImageUpload5 = bo.answerImageUpload5;
        answerName = bo.answerName;
        answerDescription = bo.answerDescription;
        answerImageThumbnail = bo.answerImageThumbnail;
        answerImageFeature = bo.answerImageFeature;
        answerDisplayType = bo.answerDisplayType;
        answerPriority = bo.answerPriority;
    }
}

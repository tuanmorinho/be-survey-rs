package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.obj.PriorityObject;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_ANSWER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurveyAnswer extends AbstractBo implements Bo<Integer>, PriorityObject {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_QUESTION_ID")
    @JsonIgnore
    private SurveyQuestion surveyQuestion;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @Column(name = "IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String imageThumbnail;

    @Column(name = "IMAGE_FEATURE", columnDefinition = "TEXT")
    private String imageFeature;

    @Column(name = "DISPLAY_TYPE")
    private Integer displayType;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "DEMAND_SCORE")
    private Integer demandScore;

    public SurveyAnswer() {
    }

    public SurveyAnswer(Integer id) {
        this.id = id;
    }

    public static final String _surveyQuestion = "surveyQuestion";
    public static final String _name = "name";
    public static final String _description = "description";
    public static final String _imageThumbnail = "imageThumbnail";
    public static final String _imageFeature = "imageFeature";
    public static final String _displayType = "displayType";
    public static final String _priority = "priority";
    public static final String _demandScore = "demandScore";

    @Override
    public boolean equalsData(Object object) {
        SurveyAnswer bo = (SurveyAnswer) object;
        if (!BeanUtils.nullSafeEquals(surveyQuestion.getId(), bo.surveyQuestion.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(name, bo.name)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(description, bo.description)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageThumbnail, bo.imageThumbnail)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(imageFeature, bo.imageFeature)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(displayType, bo.displayType)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(priority, bo.priority)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(demandScore, bo.demandScore);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof SurveyAnswer bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        surveyQuestion = bo.surveyQuestion;
        name = bo.name;
        description = bo.description;
        imageThumbnail = bo.imageThumbnail;
        imageFeature = bo.imageFeature;
        displayType = bo.displayType;
        priority = bo.priority;
        demandScore = bo.demandScore;
    }
}

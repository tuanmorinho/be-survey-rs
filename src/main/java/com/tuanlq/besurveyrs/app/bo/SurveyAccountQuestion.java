package com.tuanlq.besurveyrs.app.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tuanlq.besurveyrs.infrastructure.core.bo.AbstractBo;
import com.tuanlq.besurveyrs.infrastructure.core.bo.Bo;
import com.tuanlq.besurveyrs.infrastructure.exception.MyException;
import com.tuanlq.besurveyrs.infrastructure.core.utils.BeanUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_SURVEY_ACCOUNT_QUESTION")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurveyAccountQuestion extends AbstractBo implements Bo<Integer> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ACCOUNT_ID")
    @JsonIgnore
    private SurveyAccount surveyAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_QUESTION_ID")
    @JsonIgnore
    private SurveyQuestion surveyQuestion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    @JsonIgnore
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    @JsonIgnore
    private Account account;

    @Column(name = "STEP_INDEX")
    private Integer stepIndex;

    @Column(name = "QUESTION_NAME", nullable = false)
    private String questionName;

    @Column(name = "QUESTION_DESCRIPTION", columnDefinition = "TEXT")
    private String questionDescription;

    @Column(name = "QUESTION_IMAGE_THUMBNAIL", columnDefinition = "TEXT")
    private String questionImageThumbnail;

    @Column(name = "QUESTION_IMAGE_FEATURE", columnDefinition = "TEXT")
    private String questionImageFeature;

    @Column(name = "QUESTION_ANSWER_DISPLAY_TYPE")
    private Integer questionAnswerDisplayType;

    @Column(name = "QUESTION_ANSWER_SELECT_TYPE")
    private Integer questionAnswerSelectType;

    @Column(name = "QUESTION_IS_ANSWER_REQUIRED")
    private Boolean questionIsAnswerRequired;

    @Column(name = "QUESTION_IS_FIRST")
    private Boolean questionIsFirst;

    public static final String _surveyAccount = "surveyAccount";
    public static final String _surveyQuestion = "surveyQuestion";
    public static final String _survey = "survey";
    public static final String _account = "account";
    public static final String _stepIndex = "stepIndex";
    public static final String _questionIsFirst = "questionIsFirst";

    @Override
    public boolean equalsData(Object object) {
        SurveyAccountQuestion bo = (SurveyAccountQuestion) object;
        if (!BeanUtils.nullSafeEquals(surveyAccount.getId(), bo.surveyAccount.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(surveyQuestion.getId(), bo.surveyQuestion.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(survey.getId(), bo.survey.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(account.getId(), bo.account.getId())) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(stepIndex, bo.stepIndex)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionName, bo.questionName)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionDescription, bo.questionDescription)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionImageThumbnail, bo.questionImageThumbnail)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionImageFeature, bo.questionImageFeature)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionAnswerDisplayType, bo.questionAnswerDisplayType)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionAnswerSelectType, bo.questionAnswerSelectType)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(questionIsAnswerRequired, bo.questionIsAnswerRequired)) {
            return false;
        }
        return BeanUtils.nullSafeEquals(questionIsFirst, bo.questionIsFirst);
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof SurveyAccountQuestion bo)) {
            throw new MyException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        super.copyData(bo);
        surveyAccount = bo.surveyAccount;
        surveyQuestion = bo.surveyQuestion;
        survey = bo.survey;
        account = bo.account;
        stepIndex = bo.stepIndex;
        questionName = bo.questionName;
        questionDescription = bo.questionDescription;
        questionImageThumbnail = bo.questionImageThumbnail;
        questionImageFeature = bo.questionImageFeature;
        questionAnswerDisplayType = bo.questionAnswerDisplayType;
        questionAnswerSelectType = bo.questionAnswerSelectType;
        questionIsAnswerRequired = bo.questionIsAnswerRequired;
        questionIsFirst = bo.questionIsFirst;
    }
}

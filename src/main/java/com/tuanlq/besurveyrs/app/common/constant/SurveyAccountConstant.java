package com.tuanlq.besurveyrs.app.common.constant;

public class SurveyAccountConstant {
    public static final Integer ACTION_STATUS_SURVEY_NOT_STARTED = 1; //Chưa nhập survey
    public static final Integer ACTION_STATUS_SURVEY_IN_PROGRESS = 2; //Đang nhập survey
    public static final Integer ACTION_STATUS_SURVEY_COMPLETED = 3; //Đã hoàn thành survey
}

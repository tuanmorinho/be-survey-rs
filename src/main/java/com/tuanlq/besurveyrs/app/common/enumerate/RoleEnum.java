package com.tuanlq.besurveyrs.app.common.enumerate;

public enum RoleEnum {
    USER,
    ADMIN,
    ROOT,
    ACCOUNT,
    SURVEY,
    MEDIA,
    FILE,
    DEFAULT
}

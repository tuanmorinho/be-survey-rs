package com.tuanlq.besurveyrs.app.common.constant;

public class AccountConstant {
    public static final Integer REGISTER_TYPE_USER = 1;
    public static final Integer REGISTER_TYPE_ADMIN = 2;
}

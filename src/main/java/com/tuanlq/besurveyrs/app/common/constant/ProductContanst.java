package com.tuanlq.besurveyrs.app.common.constant;

public class ProductContanst {
    public static final Integer STATUS_ACTIVE = 1; // Sản phẩm đang bán
    public static final Integer STATUS_INACTIVE = 2; // Sản phẩm ngừng bán
}

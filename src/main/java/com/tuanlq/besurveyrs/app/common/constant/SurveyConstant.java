package com.tuanlq.besurveyrs.app.common.constant;

public class SurveyConstant {
    public static final Integer STATUS_ACTIVE = 1; // Khảo sát đang hoạt động
    public static final Integer STATUS_INACTIVE = 2; // Khảo sát ngừng hoạt động

    public static final Integer NEXT_STEP_TYPE_FLAT = 1; // Loại chuyển câu hỏi dạng FLAT
    public static final Integer NEXT_STEP_TYPE_TREE = 2; // Loại chuyển câu hỏi dạng TREE
}

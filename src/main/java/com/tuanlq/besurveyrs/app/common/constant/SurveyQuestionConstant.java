package com.tuanlq.besurveyrs.app.common.constant;

import jakarta.persistence.Column;

public class SurveyQuestionConstant {
    public static final Integer ANSWER_DISPLAY_TYPE_LIST = 1; // Hiển thị kiểu cụm câu trả lời dạng LIST
    public static final Integer ANSWER_DISPLAY_TYPE_GRID = 2; // Hiển thị kiểu cụm câu trả lời dạng GRID

    public static final Integer ANSWER_SELECT_TYPE_ONE = 1; // Kiểu chọn một
    public static final Integer ANSWER_SELECT_TYPE_MULTI = 2; // Kiểu chọn nhiều
}

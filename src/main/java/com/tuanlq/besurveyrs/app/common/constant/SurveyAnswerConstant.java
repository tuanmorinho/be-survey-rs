package com.tuanlq.besurveyrs.app.common.constant;

public class SurveyAnswerConstant {
    public static final Integer DISPLAY_TYPE_SELECT = 1; // Hiển thị kiểu chọn (one hoặc multi)
    public static final Integer DISPLAY_TYPE_INPUT = 2; // Hiển thị kiểu nhập text
    public static final Integer DISPLAY_TYPE_TEXTAREA = 3; // Hiển thị kiểu upload ảnh
    public static final Integer DISPLAY_TYPE_PROGRESS_BAR = 4; // Hiển thị kiểu thanh kéo
}
